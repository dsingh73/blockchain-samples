package com.debut.flows.Healthcarecontract

import co.paralleluniverse.fibers.Suspendable
import com.debut.clientmodel.Healthcarecontract.UpdateStatusContract
import com.debut.contracts.HealthcareCommand
import com.debut.contracts.RegistrationContract
import com.debut.flows.BaseFlow
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.flows.*
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList


object FlowUpdateContractStatusFlow {
    @InitiatingFlow
    @StartableByRPC
    class UpdateContractStatus(
            val request: UpdateStatusContract

    ) : BaseFlow<String>() {
        override val progressTracker = ProgressTracker()

        @Suspendable
        override fun call(): String {


            // We retrieve the notary identity from the network map.
            val notary = firstNotary

            //get current item
            val currentItemState = getContract(request.contractId)


            // We create a transactioncd  builder and add the components.
            val txBuilder = TransactionBuilder(notary = notary)

            var message = "Contract status updated."
            //if it's cancelled request consume the existing state
            if (request.status == CONTRACTSTATUS.CANCELLED.value) {
                //update contract command
                val command = Command(HealthcareCommand.CancelContract(),
                        ourIdentity.owningKey)
                txBuilder.withItems(
                        currentItemState,
                        command
                )
                message = "Contract cancelled."
            } else {

//                //verify input for
//                if(request.note==null || request.note.isEmpty())
//                {
//                    throw IllegalStateException("Please provide note")
//                }
//               else  if(request.updateSections==null || request.updateSections.isEmpty())
//                {
//                    throw IllegalStateException("Please enter update section")
//                }


                //update contract command
                val command = Command(HealthcareCommand.UpdateContractStatus(),
                        ourIdentity.owningKey)

                //get current note
                val currentNoteList = currentItemState.state.data.contractNote
                //add new note message
                val updatedNoteList = ArrayList<String>()
                updatedNoteList.addAll(currentNoteList?.asIterable() ?: ArrayList())
                updatedNoteList.add(request.note ?: "")
                //update status with new note
                val newItemState = currentItemState.state.data.copyOfDataWithNewStatus(
                        request.status,
                        formateDate(Date()),
                        updatedNoteList,
                        request.updateSections
                )

                txBuilder.withItems(
                        currentItemState,
                        StateAndContract(newItemState, RegistrationContract.ID),
                        command
                )
            }

            try {
                //finalize the transaction
                finaliseTx(txBuilder)
            } catch (ex: Exception) {
                throw IllegalStateException(ex.localizedMessage)
            }
            return message

        }
    }

    // Replace Responder's definition with:
    @InitiatedBy(FlowUpdateContractStatusFlow.UpdateContractStatus::class)
    class IssueResponder(private val otherPartySession: FlowSession) : FlowLogic<Unit>() {
        @Suspendable
        override fun call() {
            subFlow(ReceiveFinalityFlow(otherPartySession))
        }
    }
}


