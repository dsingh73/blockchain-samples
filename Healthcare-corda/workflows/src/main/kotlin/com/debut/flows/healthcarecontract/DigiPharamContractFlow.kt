package com.debut.flows.Healthcarecontract

import co.paralleluniverse.fibers.Suspendable
import com.debut.clientmodel.Healthcarecontract.HealthcarehramContractModel
import com.debut.contracts.HealthcareCommand
import com.debut.contracts.RegistrationContract
import com.debut.flows.BaseFlow
import com.debut.flows.TypesConverter
import com.debut.states.Healthcarecontract.*
import net.corda.core.contracts.Command
import net.corda.core.flows.*
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import java.util.*
import kotlin.collections.ArrayList

object HealthcareContractFlow {
    @InitiatingFlow
    @StartableByRPC

    class AddHealthcareContract(
            val request: HealthcarehramContractModel
    ) : BaseFlow<String>() {
        override val progressTracker = ProgressTracker()

        @Suspendable
        override fun call(): String {


//generate population model
            val population = Population(
                    request.population?.shortDesc,
                    request.population?.gender,
                    request.population?.genderSpecify,
                    request.population?.country,
                    TypesConverter.fromArrayListToString(request.population?.regions ?: ArrayList()),
                    request.population?.ageFrom,
                    request.population?.ageTo
            )

//generate pricing model
            val pricing = Pricing(
                    request.pricing?.description,
                    request.pricing?.basePrice,
                    request.pricing?.currency,
                    TypesConverter.fromArrayListToString<PricingRange>(request.pricing?.rangeList ?: ArrayList())
            )
//generate validity model
            val validity = Validity(
                    request.validity.contractStartDate,
                    TypesConverter.fromArrayListToString(request.validity.validityType ?: ArrayList()),
                    request.validity.validFrom,
                    request.validity.validTo,
                    request.validity.budgetAmount,
                    request.validity.numberOfCourses,
                    request.validity.treatmentVolume,
                    request.validity.budgetImpact,
                    request.validity.period,
                    request.validity.frequency

            )


            //aggregation level model
            val aggregationLevel = AggregationLevel(
                    request.aggregationLevel?.aggDescription,
                    request.aggregationLevel?.aggGender,
                    request.aggregationLevel?.aggGenderSpecify,
                    request.aggregationLevel?.aggAgeFrom,
                    request.aggregationLevel?.aggAgeTo,
                    request.aggregationLevel?.aggCountry,
                    TypesConverter.fromArrayListToString(request.aggregationLevel?.aggregions ?: ArrayList()),
                    request.aggregationLevel?.bmiFrom,
                    request.aggregationLevel?.bmiTo,
                    request.aggregationLevel?.numberOfEvents,
                    request.aggregationLevel?.numberOfPrescriptions,
                    request.aggregationLevel?.patientByPatient

            )

           //create contract history
            val contractHistory = ContractHistory("Version", 1, formateDate(Date()))


            //check if contract is final or not
            if(request.contractStatus==CONTRACTSTATUS.FINAL.value) {
                //create separate contract for each payer
                request.payerId?.let {
                    if (it.isNotEmpty()) {
                        for (id in it) {
                            //create contract
                            createContract(
                                    id,
                                    validity,
                                    pricing,
                                    aggregationLevel,
                                    population,
                                    contractHistory
                            )
                        }
                    }
                }
            }


            return "Contract created"


        }

        @Suspendable
        //create contract for a user
        private fun createContract(
                payerId: String,
                validity: Validity,
                pricing: Pricing,
                aggregationLevel: AggregationLevel,
                population: Population,
                contractHistory: ContractHistory
        ) {

            // We retrieve the notary identity from the network map.
            val notary = firstNotary
            //create unique access key for each user to validate api request
            val contractId = UUID.randomUUID()
            //create output state for contract
            val outputState = HealthcareContractState(
                    contractId.toString(),
                    request.contractType,     // 1-Volume, 2- Outcome, 3- Rebate, 4- Refund
                    request.contractName,
                    request.contractDescription,
                    payerId,
                    request.contractVersion,
                    request.contractNote,
                    ArrayList(),            //set blank update sections
                    request.contractStatus, // PENDING, REJECTED, ACCEPTED, SIGNED_OFF, OPEN, CANCELLED, RUNNING, CLOSED
                    request.createdBy,     // user unique id
                    request.manufacturerId, // unique manufacturer id
                    validity,              //
                    request.treatmentId,  // unique treatmentId id
                    request.outcomeId,   //  unique outComeId id
                    pricing,            // pricing model
                    aggregationLevel,   //add aggregation
                    population,
                    request.periodicity,
                    request.adverseEvent,
                    contractHistory,    //add contract history
                    request.healthCareProfessional,
                    formateDate(Date()),
                    formateDate(Date()),
                    getPartyList()
            )

//registration command will be verified by issuercontract
            val command = Command(HealthcareCommand.CreateContract(),
                    ourIdentity.owningKey)

// We create a transaction builder and add the components.
            val txBuilder = TransactionBuilder(notary = notary)
                    .addOutputState(outputState, RegistrationContract.ID)
                    .addCommand(command)

            //finalize the transaction
            finaliseTx(txBuilder)


        }


        // Replace Responder's definition with:
        @InitiatedBy(HealthcareContractFlow.AddHealthcareContract::class)
        class IssueResponder(private val otherPartySession: FlowSession) : FlowLogic<Unit>() {
            @Suspendable
            override fun call() {
                subFlow(ReceiveFinalityFlow(otherPartySession))
            }
        }


    }
}


