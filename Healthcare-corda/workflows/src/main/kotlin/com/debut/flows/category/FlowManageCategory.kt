package com.debut.flows.category

import co.paralleluniverse.fibers.Suspendable
import com.debut.clientmodel.category.ManageCategory
import com.debut.contracts.HealthcareCommand
import com.debut.contracts.RegistrationContract
import com.debut.flows.BaseFlow
import com.debut.states.category.AssetCategoryState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.flows.*
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import java.time.Instant

object FlowManageCategory {
    @InitiatingFlow
    @StartableByRPC

    class Category(

            val request: ManageCategory
    ) : BaseFlow<String>() {
        override val progressTracker = ProgressTracker()
        @Suspendable
        override fun call(): String {


// We retrieve the notary identity from the network map.
            val notary = firstNotary


//registration command will be verified by issuercontract
            val command = Command(HealthcareCommand.Timer(),
                    ourIdentity.owningKey)

// We create a transaction builder and add the components.
            val txBuilder = TransactionBuilder(notary = notary)





               return  when (request.type) {
                    CATEGORYSTATEENUM.CREATE.value -> {
                        val categoryMap = mutableMapOf(
                                1 to "Watches",
                                2 to "Diamonds",
                                3 to "Cars",
                                4 to "Gold",
                                5 to "Fine Arts",
                                6 to "Wine",
                                7 to "Collectables",
                                8 to "Coins",
                                9 to "Antiques"
                        )
                        val outputState = AssetCategoryState(categoryMap, Instant.now(), getPartyList())
                        txBuilder.withItems(
                                StateAndContract(outputState, RegistrationContract.ID),
                                command
                        )

                        //finalize the transaction
                        finaliseTx(txBuilder)

                        "All category added"

                    }
                    CATEGORYSTATEENUM.ADDNEW.value -> {

                        //get current category state
                        val criteria = QueryCriteria.VaultQueryCriteria()
                        val categoryCurrentStateRef = serviceHub.vaultService.queryBy(AssetCategoryState::class.java, criteria)
                                .states.firstOrNull() ?: throw IllegalStateException("No previous category found!")

                        val categoryMap = categoryCurrentStateRef.state.data.categoryMap
                        if(categoryMap.isNotEmpty()) {
                            //next category index
                            val nextKey=categoryMap.size+1
                            //create new category map
                            val newCategory= mutableMapOf<Int,String>()
                            //add all previous and new key to map and generate next state
                            newCategory.putAll(categoryMap)
                            newCategory[nextKey]=request.categoryName
                            val categoryNewState = categoryCurrentStateRef.state.data.updateCategory(newCategory)

                            txBuilder.withItems(
                                    categoryCurrentStateRef,
                                    StateAndContract(categoryNewState, RegistrationContract.ID),
                                    command
                            )

                            //finalize the transaction
                            finaliseTx(txBuilder)
                        }
                        "Category updated"

                    }
                    CATEGORYSTATEENUM.DELETE.value -> {
                        //get current category state
                        val criteria = QueryCriteria.VaultQueryCriteria()
                        val categoryCurrentStateRef = serviceHub.vaultService.queryBy(AssetCategoryState::class.java, criteria)
                                .states.firstOrNull() ?: throw IllegalStateException("No previous category found!")


                        val categoryMap = categoryCurrentStateRef.state.data.categoryMap
                        var keyValue = 0
                        for ((key, value) in categoryMap) {
                            if (value.toUpperCase() == request.categoryName.toUpperCase()) {
                                keyValue = key
                                break
                            }
                        }


                        //check if key found for deleting value
                        if (keyValue > 0) {
                            //create new category map
                            val newCategory= mutableMapOf<Int,String>()
                            //add all previous and new key to map and generate next state
                            newCategory.putAll(categoryMap)
                            //add new
                            newCategory.remove(keyValue)

                            val categoryNewState = categoryCurrentStateRef.state.data.updateCategory(newCategory)

                            txBuilder.withItems(
                                    categoryCurrentStateRef,
                                    StateAndContract(categoryNewState, RegistrationContract.ID),
                                    command
                            )
                            //finalize the transaction
                            finaliseTx(txBuilder)
                        }

                        "Category deleted"

                    }
                    else -> throw IllegalStateException("Invalid type")

                }


        }
    }


    // Replace Responder's definition with:
    @InitiatedBy(FlowManageCategory.Category::class)
    class IssueResponder(private val otherPartySession: FlowSession) : FlowLogic<Unit>() {
        @Suspendable
        override fun call() {
            subFlow(ReceiveFinalityFlow(otherPartySession))
        }
    }


}