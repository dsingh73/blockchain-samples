package com.debut.flows.user

import co.paralleluniverse.fibers.Suspendable
import com.debut.clientmodel.graphdata.DashboardDataModel
import com.debut.flows.BaseFlow
import com.debut.utility.Utils
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC


object UserDashboardFlow {
    @InitiatingFlow
    @StartableByRPC
    class UserDashboard(
            val userId: String

    ) : BaseFlow<DashboardDataModel>() {

        @Suspendable
        override fun call(): DashboardDataModel {

            // Connecting to the node's database.
            val session = serviceHub.jdbcSession()

            val dataList = Utils.getPreviousAndCurrentWeekDates()
            val currentWeekStartEndDates = dataList[0]
            val previousWeekStartEndDates = dataList[1]

            val rawQuery =

                    """
                select 
                      (
                    select
                    count(contractSchema.*) 
                from
                    vault_states vaultSchema
                join
                    contract contractSchema
                where
                    vaultSchema.output_index=contractSchema.output_index
                    and vaultSchema.transaction_id=contractSchema.transaction_id
                    and vaultSchema.state_status=0
                     and contractSchema.contractstatus=${CONTRACTSTATUS.ACCEPTED.value}
                   ) as activeContracts,
                   
                   (
                    select
                    count(contractSchema.*) 
                from
                    vault_states vaultSchema
                join
                    contract contractSchema
                where
                    vaultSchema.output_index=contractSchema.output_index
                    and vaultSchema.transaction_id=contractSchema.transaction_id
                    and vaultSchema.state_status=0
                     and contractSchema.contractstatus in (${CONTRACTSTATUS.AMENDED.value},${CONTRACTSTATUS.CORRECTED.value})
                   ) as pendingContracts,
                   
                   (
                    select
                    count(contractSchema.*) 
                from
                    vault_states vaultSchema
                join
                    contract contractSchema
                where
                    vaultSchema.output_index=contractSchema.output_index
                    and vaultSchema.transaction_id=contractSchema.transaction_id
                    and vaultSchema.state_status=0
                    and contractSchema.contractCreatedAt  between '${previousWeekStartEndDates.first}' and '${previousWeekStartEndDates.second}' 
                    and contractSchema.contractstatus=${CONTRACTSTATUS.ACCEPTED.value}
                   ) as openLastWeekTotal,
        
                  (
                    select
                    count(contractSchema.*) 
                from
                    vault_states vaultSchema
                join
                    contract contractSchema
                where
                    vaultSchema.output_index=contractSchema.output_index
                    and vaultSchema.transaction_id=contractSchema.transaction_id
                    and vaultSchema.state_status=0
                    and contractSchema.contractCreatedAt  between '${previousWeekStartEndDates.first}' and '${previousWeekStartEndDates.second}' 
                    and contractSchema.contractstatus in (${CONTRACTSTATUS.FINAL.value},${CONTRACTSTATUS.AMENDED.value},${CONTRACTSTATUS.CORRECTED.value})
                   ) as pendingLastWeekTotal,
        
        
                    (
                    select
                    count(contractSchema.*) 
                from
                    vault_states vaultSchema
                join
                    contract contractSchema
                where
                    vaultSchema.output_index=contractSchema.output_index
                    and vaultSchema.transaction_id=contractSchema.transaction_id
                    and vaultSchema.state_status=0
                    and contractSchema.contractCreatedAt  between '${currentWeekStartEndDates.first}' and '${currentWeekStartEndDates.second}' 
                    and contractSchema.contractstatus=${CONTRACTSTATUS.ACCEPTED.value}
                
                   ) as openCurrentWeekTotal,
                   
                   (
                    select
                    count(contractSchema.*) 
                from
                    vault_states vaultSchema
                join
                    contract contractSchema
                where
                    vaultSchema.output_index=contractSchema.output_index
                    and vaultSchema.transaction_id=contractSchema.transaction_id
                    and vaultSchema.state_status=0
                    and contractSchema.contractCreatedAt  between '${currentWeekStartEndDates.first}' and '${currentWeekStartEndDates.second}' 
                    and contractSchema.contractstatus in (${CONTRACTSTATUS.AMENDED.value},${CONTRACTSTATUS.AMENDED.value},${CONTRACTSTATUS.CORRECTED.value})
                
                   ) as pendingCurrentWeekTotal

                
        
                    """

            val preparedStatementRead = session.prepareStatement(rawQuery)
            val result = preparedStatementRead.executeQuery()

            val dashboardDataModel = DashboardDataModel()

            result?.let {
                //calculate active contracts and growth

                while (it.next()) {


                    val lastWeekOpenContractsTotal = it.getInt("openLastWeekTotal")
                    val currentWeekOpenContractsTotal = it.getInt("openCurrentWeekTotal")
                    //formula to calculate open contract growth percentage
                    val openContractGrowthPerCent = if (lastWeekOpenContractsTotal != 0)
                        ((currentWeekOpenContractsTotal - lastWeekOpenContractsTotal) / (lastWeekOpenContractsTotal * 1.0f)) * 100
                    else
                        currentWeekOpenContractsTotal * 100 * 1.0f


                    val lastWeekPendingContractsTotal = it.getInt("pendingLastWeekTotal")
                    val currentWeekPendingContractsTotal = it.getInt("pendingCurrentWeekTotal")

                    //formula to calculate pending contract growth percentage
                    val pendingContractGrowthPerCent = if (lastWeekPendingContractsTotal != 0)
                        ((currentWeekPendingContractsTotal - lastWeekPendingContractsTotal) / (lastWeekPendingContractsTotal * 1.0f)) * 100
                    else
                        currentWeekPendingContractsTotal * 100 * 1.0f


                    dashboardDataModel.activeContracts = it.getInt("activeContracts")
                    dashboardDataModel.pendingContracts = it.getInt("pendingContracts")
                    dashboardDataModel.activeContractgrowthPercentage = openContractGrowthPerCent
                    dashboardDataModel.pendingContractgrowthPercentage = pendingContractGrowthPerCent

                }

            }
            return dashboardDataModel
        }
    }


}