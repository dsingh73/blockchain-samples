package com.debut.flows.Healthcarecontract

import co.paralleluniverse.fibers.Suspendable
import com.debut.clientmodel.Healthcarecontract.HealthcareaContractDetailsResponseModel
import com.debut.flows.BaseFlow
import com.debut.services.CordaDatabaseService
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.utilities.ProgressTracker

object GetContractContractFlow {
    @InitiatingFlow
    @StartableByRPC
    class Contract(
            val contractId: String,
            val userId: String
    ) : BaseFlow<HealthcareaContractDetailsResponseModel?>() {

        override val progressTracker = ProgressTracker()
        @Suspendable
        override fun call(): HealthcareaContractDetailsResponseModel? {

            val customVaultQueryService = serviceHub.cordaService(CordaDatabaseService.Service::class.java)
            return customVaultQueryService.getContract(contractId,userId)
        }
    }
}