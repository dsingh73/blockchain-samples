package com.debut.flows.user

import co.paralleluniverse.fibers.Suspendable
import com.debut.contracts.HealthcareCommand
import com.debut.flows.BaseFlow
import com.debut.utility.ErrorMessage
import com.debut.utility.Messages
import net.corda.core.contracts.Command
import net.corda.core.flows.*
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker

object FlowUserDelete {
    @InitiatingFlow(version = 2)
    @StartableByRPC

    class UserDelete(
            val userId: String

    ) : BaseFlow<String>() {
        override val progressTracker = ProgressTracker()
        @Suspendable
        override fun call(): String {



// We retrieve the notary identity from the network map.
            val notary = firstNotary


//registration command will be verified by issuercontract
            val command = Command(HealthcareCommand.DeleteUser(),
                    ourIdentity.owningKey)

// We create a transaction builder and add the components.
            val txBuilder = TransactionBuilder(notary = notary)


//get current user state
            val userCurrentStateRef = checkUserExists(userId)

            txBuilder.withItems(
                    userCurrentStateRef,
                    command
            )


            //finalize the transaction
            finaliseTx(txBuilder)

            return Messages.PROFILE_DELETED
        }
    }


    // Replace Responder's definition with:
    @InitiatedBy(FlowUserDelete.UserDelete::class)
    class IssueResponder(private val otherPartySession: FlowSession) : FlowLogic<Unit>() {
        @Suspendable
        override fun call() {
            subFlow(ReceiveFinalityFlow(otherPartySession))
        }
    }


}