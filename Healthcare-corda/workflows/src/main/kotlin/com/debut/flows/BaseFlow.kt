package com.debut.flows


import co.paralleluniverse.fibers.Suspendable

import com.debut.states.category.AssetCategoryState
import com.debut.states.Healthcarecontract.HealthcareContractState
import com.debut.states.Healthcarecontract.HealthcareContractSchema
import com.debut.states.disease.DiseaseSchema
import com.debut.states.disease.DiseaseState

import com.debut.states.more.TimerState
import com.debut.states.outcome.OutcomeState
import com.debut.states.outcome.OutcomeStateSchema
import com.debut.states.treatment.TreatmentSchema
import com.debut.states.treatment.TreatmentState
import com.debut.states.user.UserSchema
import com.debut.states.user.UserState
import com.debut.utility.ErrorMessage
import com.debut.utility.RandomString
import com.debut.utility.Utils
import net.corda.core.contracts.StateAndRef
import net.corda.core.crypto.SecureHash
import net.corda.core.flows.*
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.builder
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit
import java.util.*


/**
 * An abstract FlowLogic class that is subclassed by the obligation flows to
 * provide helper methods and classes.
 */
abstract class BaseFlow<out T> : FlowLogic<T>() {


    val firstNotary
        get() = serviceHub.networkMapCache.notaryIdentities.firstOrNull()
                ?: throw FlowException(ErrorMessage.NO_AVAILABLE_NOTARY)


    fun getPartyByName(partyName: String, rpcOps: CordaRPCOps): Party {
        println("party by name ====" + partyName)

        val lenderIdentity = rpcOps.partiesFromName(partyName, exactMatch = false).singleOrNull()
                ?: throw IllegalStateException("Couldn't lookup node identity for $partyName.")
        return lenderIdentity
    }


    fun resolveIdentity(abstractParty: AbstractParty): Party {
        return serviceHub.identityService.requireWellKnownPartyFromAnonymous(abstractParty)
    }


    companion object {
        val PROSPECTUS_HASH = SecureHash.parse("decd098666b9657314870e192ced0c3519c2c9d395507a238338f8d003929de9")

        object SELF_ISSUING : ProgressTracker.Step("Got session ID back, issuing and timestamping some commercial paper")

        object TRADING : ProgressTracker.Step("Starting the traderequest flow") {
            //  override fun childProgressTracker(): ProgressTracker = TwoPartyTradeFlow.Seller.tracker()
        }

        //        object ISSER_NEW_STATE : ProgressTracker.Step("Issuer new state updated")
//
//        object BUYER_STATE_CREATED : ProgressTracker.Step("Buyer state generated")
//
//        object TRADE_COMPLETED : ProgressTracker.Step("Trading completed")
//
//
//        // We vend a progress tracker that already knows there's going to be a TwoPartyTradingFlow involved at some
        // point: by setting up the tracker in advance, the user can see what's coming in more detail, instead of being
        // surprised when it appears as a new set of tasks below the current one.
        fun tracker() = ProgressTracker(SELF_ISSUING, TRADING)

        val queryCriteriaLimit = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.ALL)
        val queryCriteriaLimitConsumed = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.CONSUMED)
        val queryCriteriaLimitUnconsumed = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.UNCONSUMED)
        const val DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"

    }

    fun checkUserExists(userId: String): StateAndRef<UserState> {
        //only to get unconsumed state
        val queryCriteriaLimit = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.UNCONSUMED)
        //check for investor
        val userSecurityType = builder { UserSchema.PersistentUserState::userId.equal(userId) }

        val criteria = QueryCriteria.VaultCustomQueryCriteria(userSecurityType).and(queryCriteriaLimit)
        return serviceHub.vaultService.queryBy(UserState::class.java, criteria)
                .states.firstOrNull() ?: throw IllegalStateException(ErrorMessage.NO_USER_FOUND)

    }


    //validate dates
    fun getDate(offerEndDate: String): Date {
        return try {
            SimpleDateFormat(DATE_FORMAT).parse(offerEndDate)
        } catch (e: ParseException) {
            throw IllegalStateException(ErrorMessage.DATE_FORMAT_ISSUE)
        }

    }

    fun formateDate(date: Date): Instant {

        val ins = Instant.ofEpochMilli(date.time)
        val odt = ins.atOffset(ZoneOffset.UTC)
        return Instant.from(odt) //return utc

   }

    fun getHoldingDate(country: String, endDate: Date): Date {
        val calendar = Calendar.getInstance()
        calendar.time = endDate

        return when (country) {
            CountryAttribute.USA.value -> {
                ExemptionDurationAttribute.USA
                calendar.add(Calendar.MONTH, ExemptionDurationAttribute.USA.value)
                calendar.time
            }
            CountryAttribute.INDIA.value -> {
                ExemptionDurationAttribute.INDIA
                calendar.add(Calendar.MONTH, ExemptionDurationAttribute.INDIA.value)
                calendar.time
            }
            else -> {
                throw IllegalStateException(ErrorMessage.HOLDING_DATE_FOR_INVESTOR)
            }
        }
    }

    //finalize the transactions and save them to corresponding vaults
    @Suspendable
    protected fun finaliseTx(txBuilder: TransactionBuilder): SignedTransaction {
        try {

            // Verifying the transaction.
            txBuilder.verify(serviceHub)

// Signing the transaction. from sender side
            val signedTx = serviceHub.signInitialTransaction(txBuilder)

            val listOfSession = getFlowSession()
//end the flow and write the transation data to related parties vault
            subFlow(FinalityFlow(signedTx, listOfSession))

            return subFlow(FinalityFlow(signedTx, listOfSession))
        } catch (e: Exception) {
            throw IllegalStateException(e.localizedMessage)
        }
    }

    protected fun getPartyList(): List<Party> {
        // We create the transaction components.
        //create list of all parties involved in this flow
        val partList = ArrayList<Party>()
        partList.add(ourIdentity)

        return partList
    }

    private fun getFlowSession(): ArrayList<FlowSession> {

        val partList = getPartyList()
        val listOfSession = ArrayList<FlowSession>()
        //creating all other party session to get
        //signature from to validate the transaction
        for (index in 1 until partList.size) {
            // Creating a session with the other party.
            val otherPartySession = initiateFlow(partList[index])
            listOfSession.add(otherPartySession)
        }
        return listOfSession
    }

    protected fun nextScheduleTime(): Instant {
        //calculate next schedule time
        return Instant.now().plus(getTimer(), ChronoUnit.MINUTES) //next schedule time for interest

    }


    //Exemption based tenure
    enum class ExemptionDurationAttribute(val value: Int) {
        USA(12),
        INDIA(6),
    }


    enum class CountryAttribute(val value: String) {
        USA("USA"),
        INDIA("INDIA"),
    }


    //update category enum
    enum class CATEGORYSTATEENUM(val value: Int) {
        CREATE(1),
        ADDNEW(2),
        DELETE(3),

    }

    // PENDING, REJECTED, ACCEPTED, SIGNED_OFF, OPEN, CANCELLED, RUNNING, CLOSED
    //contract status
    enum class CONTRACTSTATUS(val value: Int) {

        DRAFT(1),
        FINAL(2),
        AMENDED(3),
        CORRECTED(4),
        CANCELLED(5),
        ACCEPTED(6)


    }

    //user role
    enum class USERROLE(val value: Int) {
        ADMIN(1),
        MANUFACTURER(2),
        PAYER(3),
        PARTICIPANT(4)
    }


    fun getNewDate(days: Int): Date {
        val currentDate = Date()
        // convert date to calendar
        val cal = Calendar.getInstance()
        cal.time = currentDate
        cal.add(Calendar.DAY_OF_MONTH, days)
        return cal.time
    }


    fun getTimer(): Long {
        val criteria = QueryCriteria.VaultQueryCriteria()

        val result = serviceHub.vaultService.queryBy(TimerState::class.java, criteria)
                .states.firstOrNull()

        return result?.let {
            it.state.data.time
        } ?: Utils.EVENT_SCHEDULE_TIME_IN_DAYS

    }


    fun getVisibleTransactionId(): String {
        return RandomString().nextString()
    }


    //get the all assets category
    protected fun getCategory(): Map<Int, String> {
        //get current category state
        val criteria = QueryCriteria.VaultQueryCriteria()
        val categoryCurrentStateRef = serviceHub.vaultService.queryBy(AssetCategoryState::class.java, criteria)
                .states.firstOrNull() ?: throw IllegalStateException("No category found!")
        return categoryCurrentStateRef.state.data.categoryMap

    }

    //get previous state of treatment
    fun getTreatment(treatmentId: String): StateAndRef<TreatmentState> {
        //get the previous state of item
        val userState = builder { TreatmentSchema.PersistentTreatmentState::treatmentId.equal(treatmentId) }

        val criteria = QueryCriteria.VaultCustomQueryCriteria(userState)

        return serviceHub.vaultService.queryBy(TreatmentState::class.java, criteria)
                .states.firstOrNull()
                ?: throw IllegalStateException("No Treatment found for the  treatmentId=${treatmentId}")
    }

    //check if given disease id exists
    fun checkIfDiseaseExists(diseaseId: String): StateAndRef<DiseaseState> {
        //get the previous state of item
        val userState = builder { DiseaseSchema.PersistentDiseaseState::diseaseId.equal(diseaseId) }

        val criteria = QueryCriteria.VaultCustomQueryCriteria(userState)

        return serviceHub.vaultService.queryBy(DiseaseState::class.java, criteria)
                .states.firstOrNull()
                ?: throw IllegalStateException("No Disease found for the  diseaseId=${diseaseId}")
    }


    //check if given outcome  exists
    fun getOutcome(outcomeId: String): StateAndRef<OutcomeState> {
        //get the previous state of item
        val userState = builder { OutcomeStateSchema.PersistentOutcome::id.equal(outcomeId) }

        val criteria = QueryCriteria.VaultCustomQueryCriteria(userState)

        return serviceHub.vaultService.queryBy(OutcomeState::class.java, criteria)
                .states.firstOrNull()
                ?: throw IllegalStateException("No outcome found for the  outcomeId=${outcomeId}")
    }


    //check if given contract  exists
    fun getContract(contractId: String): StateAndRef<HealthcareContractState> {
        //get the previous state of item
        val userState = builder { HealthcareContractSchema.PersistentHealthcareContract::contractId.equal(contractId) }

        val criteria = QueryCriteria.VaultCustomQueryCriteria(userState)

        return serviceHub.vaultService.queryBy(HealthcareContractState::class.java, criteria)
                .states.firstOrNull()
                ?: throw IllegalStateException("No contract found for the  contractId=${contractId}")
    }

    //check if given contract  exists with draft
    fun getDraftContract(contractId: String): StateAndRef<HealthcareContractState>? {
        //get the previous state of item
        val userState = builder { HealthcareContractSchema.PersistentHealthcareContract::contractId.equal(contractId) }
        val userState2 = builder { HealthcareContractSchema.PersistentHealthcareContract::contractStatus.equal(CONTRACTSTATUS.DRAFT.value) }

        val criteria = QueryCriteria.VaultCustomQueryCriteria(userState).and(QueryCriteria.VaultCustomQueryCriteria(userState2))

        return serviceHub.vaultService.queryBy(HealthcareContractState::class.java, criteria)
                .states.firstOrNull()

    }

    //check if given contract exists for user or not
    fun getContractById(contractId: String, payerid: String): StateAndRef<HealthcareContractState> {
        //get the previous state of item
        val userState = builder { HealthcareContractSchema.PersistentHealthcareContract::contractId.equal(contractId) }
        val userState2 = builder { HealthcareContractSchema.PersistentHealthcareContract::payerId.equal(payerid) }

        val criteria = QueryCriteria.VaultCustomQueryCriteria(userState).and(QueryCriteria.VaultCustomQueryCriteria(userState2))

        return serviceHub.vaultService.queryBy(HealthcareContractState::class.java, criteria)
                .states.firstOrNull()
                ?: throw IllegalStateException("No contract found for the  contractId=${contractId}")
    }


}




