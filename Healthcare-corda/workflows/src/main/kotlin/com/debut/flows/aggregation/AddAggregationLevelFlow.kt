package com.debut.flows.aggregation

import co.paralleluniverse.fibers.Suspendable
import com.debut.clientmodel.aggregation.AggregationModel
import com.debut.contracts.HealthcareCommand
import com.debut.contracts.RegistrationContract
import com.debut.flows.BaseFlow
import com.debut.states.aggregation.AggregationLevelState
import net.corda.core.contracts.Command
import net.corda.core.flows.*
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import java.util.*

object AddAggregationLevelFlow {
    @InitiatingFlow(version = 1)
    @StartableByRPC

    class AddAggregation(
            val request: AggregationModel
    ) : BaseFlow<String>() {
        override val progressTracker = ProgressTracker()

        @Suspendable
        override fun call(): String {

// We retrieve the notary identity from the network map.
            val notary = firstNotary

//create unique access key for each user to validate api request
            val uniqueId = UUID.randomUUID()

//create output state of new aggregationlevel
            val outputState = AggregationLevelState(
                    uniqueId.toString(),
                    request.ageFrom,
                    request.ageTo,
                    request.country,
                    request.gender,
                    request.regions,
                    request.bmiFrom,
                    request.bmiTo,
                    request.numberOfEvents,
                    request.numberOfPrescriptions,
                    request.patientByPatient,
                    formateDate(Date()),
                    formateDate(Date()),
                    getPartyList()
            )


//registration command will be verified by issuercontract
            val command = Command(HealthcareCommand.CreateAggregation(),
                    ourIdentity.owningKey)

// We create a transaction builder and add the components.
            val txBuilder = TransactionBuilder(notary = notary)
                    .addOutputState(outputState, RegistrationContract.ID)
                    .addCommand(command)

            //finalize the transaction
            finaliseTx(txBuilder)

            return uniqueId.toString() //send back new disease unique id
        }
    }


    // Replace Responder's definition with:
    @InitiatedBy(AddAggregationLevelFlow.AddAggregation::class)
    class IssueResponder(private val otherPartySession: FlowSession) : FlowLogic<Unit>() {
        @Suspendable
        override fun call() {
            subFlow(ReceiveFinalityFlow(otherPartySession))
        }
    }


}