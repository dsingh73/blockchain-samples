package com.debut.flows.Healthcarecontract

import co.paralleluniverse.fibers.Suspendable
import com.debut.contracts.HealthcareCommand
import com.debut.contracts.RegistrationContract
import com.debut.flows.BaseFlow
import com.debut.states.Healthcarecontract.AddObservationInContractModel
import com.debut.utility.Messages.Companion.CONTRACT_UPDATED
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.flows.*
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import java.util.*

object UpdateHealthcareContractProfessionalDataFlow {
    @InitiatingFlow
    @StartableByRPC

    class UpdateHealthcareContract(
            val request: AddObservationInContractModel
    ) : BaseFlow<String>() {
        override val progressTracker = ProgressTracker()

        @Suspendable
        override fun call(): String {

           // We retrieve the notary identity from the network map.
            val notary = firstNotary

            val contractStateRef = getContract(request.contractId)
            val contractState = contractStateRef.state.data.copyNewData()


             val observationDataList=contractState.professionalObservationData
             val newObservationDataList=ArrayList<AddObservationInContractModel>()
             observationDataList?.let { newObservationDataList.addAll(it) } //add previous data list
             newObservationDataList.add(request) //add new data model of observation

            //update new data model to contract
            contractState.professionalObservationData=newObservationDataList
            //set updated at
            contractState.contractUpdatedAt = formateDate(Date())
            //count the number of time observation data updated
            contractState.observationUpdatedCount=contractState.observationUpdatedCount+1


//registration command will be verified by issuercontract
            val command = Command(HealthcareCommand.UpdateContract(),
                    ourIdentity.owningKey)

// We ccreate a transaction builder and add the components.
            val txBuilder = TransactionBuilder(notary = notary)
            txBuilder.withItems(contractStateRef,
                    StateAndContract(contractState, RegistrationContract.ID),
                    command
            )


            //finalize the transaction
            return try {
                finaliseTx(txBuilder)
                CONTRACT_UPDATED
            } catch(ex:Exception) {
                throw IllegalStateException("Unable to add observation data!")

            }
        }


        // Replace Responder's definition with:
        @InitiatedBy(UpdateHealthcareContractProfessionalDataFlow.UpdateHealthcareContract::class)
        class IssueResponder(private val otherPartySession: FlowSession) : FlowLogic<Unit>() {
            @Suspendable
            override fun call() {
                subFlow(ReceiveFinalityFlow(otherPartySession))
            }
        }


    }
}


