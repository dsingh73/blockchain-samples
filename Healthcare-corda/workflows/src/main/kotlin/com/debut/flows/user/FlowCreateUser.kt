package com.debut.flows.user

import co.paralleluniverse.fibers.Suspendable
import com.debut.clientmodel.user.HealthcareUser
import com.debut.contracts.HealthcareCommand
import com.debut.contracts.RegistrationContract
import com.debut.flows.BaseFlow
import com.debut.states.user.UserState
import net.corda.core.contracts.Command
import net.corda.core.flows.*
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import java.util.*

object FlowCreateUser {
    @InitiatingFlow
    @StartableByRPC

    class UserRegistration(
            val request: HealthcareUser
    ) : BaseFlow<String>() {
        override val progressTracker = ProgressTracker()

        @Suspendable
        override fun call(): String {


// We retrieve the notary identity from the network map.
            val notary = firstNotary

//create unique access key for each user to validate api request
            val uniqueId = UUID.randomUUID()


//create output state of newly added user
            val outputState = UserState(
                    request._id,
                    request.email,
                    request.name,
                    request.phone,
                    request.address,
                    request.role,  //user role MANUFACTURER, PAYER, TU
                    uniqueId.toString(),
                    getPartyList()
            )


//registration command will be verified by issuercontract
            val command = Command(HealthcareCommand.CreateUser(),
                    ourIdentity.owningKey)

// We create a transaction builder and add the components.
            val txBuilder = TransactionBuilder(notary = notary)
                    .addOutputState(outputState, RegistrationContract.ID)
                    .addCommand(command)

            //finalize the transaction
            finaliseTx(txBuilder)

            return uniqueId.toString() //send back access token in response to calling end
        }
    }


    // Replace Responder's definition with:
    @InitiatedBy(FlowCreateUser.UserRegistration::class)
    class IssueResponder(private val otherPartySession: FlowSession) : FlowLogic<Unit>() {
        @Suspendable
        override fun call() {
            subFlow(ReceiveFinalityFlow(otherPartySession))
        }
    }


}