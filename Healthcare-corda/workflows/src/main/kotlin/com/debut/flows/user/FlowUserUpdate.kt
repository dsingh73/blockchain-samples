package com.debut.flows.user

import co.paralleluniverse.fibers.Suspendable
import com.debut.clientmodel.user.HealthcareUser
import com.debut.contracts.HealthcareCommand
import com.debut.contracts.RegistrationContract
import com.debut.flows.BaseFlow
import com.debut.utility.Messages
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.flows.*
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker

object FlowUserUpdate {
    @InitiatingFlow
    @StartableByRPC

    class UserUpdateInfo(
            val HealthcareUser: HealthcareUser
    ) : BaseFlow<String>() {
        override val progressTracker = ProgressTracker()
        @Suspendable
        override fun call(): String {


// We retrieve the notary identity from the network map.
            val notary = firstNotary


//registration command will be verified by issuercontract
            val command = Command(HealthcareCommand.UpdateUser(),
                    ourIdentity.owningKey)

// We create a transaction builder and add the components.
            val txBuilder = TransactionBuilder(notary = notary)

//get current user state
            //get the previous state of issued security


            val userCurrentState = checkUserExists(HealthcareUser._id)

            //update user values
            val userNextState = userCurrentState.state.data.copyNewData(
                    HealthcareUser.name,
                    HealthcareUser.email,
                    HealthcareUser.address,
                    HealthcareUser.phone)


            txBuilder.withItems(
                    userCurrentState,
                    StateAndContract(userNextState, RegistrationContract.ID),
                    command
            )


            //finalize the transaction
            finaliseTx(txBuilder)
            return Messages.PROFILE_UPDATED
        }
    }


    // Replace Responder's definition with:
    @InitiatedBy(FlowUserUpdate.UserUpdateInfo::class)
    class IssueResponder(private val otherPartySession: FlowSession) : FlowLogic<Unit>() {
        @Suspendable
        override fun call() {
            subFlow(ReceiveFinalityFlow(otherPartySession))
        }
    }


}