package com.debut.flows.Healthcarecontract

import co.paralleluniverse.fibers.Suspendable
import com.debut.clientmodel.Healthcarecontract.UpdateHealthcareaContractModel
import com.debut.contracts.HealthcareCommand
import com.debut.contracts.RegistrationContract
import com.debut.flows.BaseFlow
import com.debut.flows.TypesConverter
import com.debut.states.Healthcarecontract.ContractHistory
import com.debut.states.Healthcarecontract.PricingRange
import com.debut.utility.Messages
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.flows.*
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import java.util.*

object NewHealthcareVersionContractFlow {
    @InitiatingFlow
    @StartableByRPC

    class UpdateHealthcareContractVersion(
            val request: UpdateHealthcareaContractModel
    ) : BaseFlow<String>() {
        override val progressTracker = ProgressTracker()

        @Suspendable
        override fun call(): String {

// We retrieve the notary identity from the network map.
            val notary = firstNotary

            val contractStateRef = getContractById(request.contractId?:"", request.payerId ?: "")
            val contractState = contractStateRef.state.data.copyNewData()

            //adding value
            if (!request.contractName.isNullOrBlank()) {
                contractState.contractName = request.contractName
            }
            if (request.contractType != null) {
                contractState.contractType = request.contractType
            }
            if (!request.contractDescription.isNullOrBlank()) {
                contractState.contractDescription = request.contractDescription
            }
            if (!request.payerId.isNullOrBlank()) {
                contractState.payerId = request.payerId
            }
            if (request.contractVersion != null) {
                contractState.contractVersion = request.contractVersion
            }
            if (request.contractNote?.isNotEmpty() == true) {
                contractState.contractNote = request.contractNote
            }
            if (request.contractStatus != null) {
                contractState.contractStatus = request.contractStatus
            }
            if (!request.createdBy.isNullOrBlank()) {
                contractState.createdBy = request.createdBy
            }
            if (!request.manufacturerId.isNullOrBlank()) {
                contractState.manufacturerId = request.manufacturerId
            }

            if (!request.treatmentId.isNullOrBlank()) {
                contractState.treatmentId = request.treatmentId
            }
            if (!request.outcomeId.isNullOrBlank()) {
                contractState.outcomeId = request.outcomeId
            }

            //for pricing
            if (!request.pricing?.description.isNullOrBlank()) {
                contractState.pricing?.pricingDescription = request.pricing?.description
            }
            if (request.pricing?.basePrice != null) {
                contractState.pricing?.basePrice = request.pricing.basePrice
            }
            if (!request.pricing?.currency.isNullOrBlank()) {
                contractState.pricing?.currency = request.pricing?.currency
            }
            if (request.pricing?.rangeList?.isNotEmpty() == true) {
                contractState.pricing?.rangeList = TypesConverter.fromArrayListToString<PricingRange>(request.pricing.rangeList)
            }

            //for aggregation level data
            if (request.aggregationLevel?.aggAgeFrom != null) {
                contractState.aggregationLevel?.aggAgeFrom = request.aggregationLevel.aggAgeFrom
            }
            if (request.aggregationLevel?.aggAgeTo != null) {
                contractState.aggregationLevel?.aggAgeTo = request.aggregationLevel.aggAgeTo
            }
            if (!request.aggregationLevel?.aggCountry.isNullOrBlank()) {
                contractState.aggregationLevel?.aggCountry = request.aggregationLevel?.aggCountry
            }
            if (request.aggregationLevel?.bmiFrom != null) {
                contractState.aggregationLevel?.bmiFrom = request.aggregationLevel.bmiFrom
            }
            if (request.aggregationLevel?.bmiTo != null) {
                contractState.aggregationLevel?.bmiTo = request.aggregationLevel.bmiTo
            }
            if (request.aggregationLevel?.numberOfEvents != null) {
                contractState.aggregationLevel?.numberOfEvents = request.aggregationLevel.numberOfEvents
            }
            if (request.aggregationLevel?.numberOfPrescriptions != null) {
                contractState.aggregationLevel?.numberOfPrescriptions = request.aggregationLevel.numberOfPrescriptions
            }
            if (request.aggregationLevel?.patientByPatient != null) {
                contractState.aggregationLevel?.patientByPatient = request.aggregationLevel.patientByPatient
            }


            //for population data
            if (!request.population?.shortDesc.isNullOrBlank()) {
                contractState.population?.shortDesc = request.population?.shortDesc
            }
            if (request.population?.gender != null) {
                contractState.population?.gender = request.population.gender
            }
            if (!request.population?.country.isNullOrBlank()) {
                contractState.population?.country = request.population?.country
            }
            if (request.population?.regions?.isNotEmpty() == true) {
                contractState.population?.regions = TypesConverter.fromArrayListToString<String>(request.population.regions
                )
            }
            if (request.population?.ageFrom != null) {
                contractState.population?.ageFrom = request.population.ageFrom
            }
            if (request.population?.ageTo != null) {
                contractState.population?.ageTo = request.population.ageTo
            }


            //for periodicity
            if (!request.periodicity?.periodicityDescription.isNullOrBlank()) {
                contractState.periodicity?.periodicityDescription = request.periodicity?.periodicityDescription
            }
            if (request.periodicity?.periodvalue != null) {
                contractState.periodicity?.periodvalue = request.periodicity.periodvalue
            }
            if (request.periodicity?.periodUom != null) {
                contractState.periodicity?.periodUom = request.periodicity.periodUom
            }
            if (request.periodicity?.waitingPeriodValue != null) {
                contractState.periodicity?.waitingPeriodValue = request.periodicity.waitingPeriodValue
            }
            if (request.periodicity?.waitingPeriodUom != null) {
                contractState.periodicity?.waitingPeriodUom = request.periodicity.waitingPeriodUom
            }

            //to update professional id
            if (!request.healthCareProfessional.isNullOrBlank()) {
                contractState.healthCareProfessional = request.healthCareProfessional
            }


            //set updated at
            contractState.contractUpdatedAt = formateDate(Date())

            val newVersionNumber = contractState.contractHistory.versionNumber + 1
            //create contract history
            val contractHistory = ContractHistory("Version", newVersionNumber, formateDate(Date()))

            //set new version
            contractState.contractHistory = contractHistory


//registration command will be verified by issuercontract
            val command = Command(HealthcareCommand.UpdateContract(),
                    ourIdentity.owningKey)

// We ccreate a transaction builder and add the components.
            val txBuilder = TransactionBuilder(notary = notary)
            txBuilder.withItems(
                    contractStateRef,
                    StateAndContract(contractState, RegistrationContract.ID),
                    command
            )


            //finalize the transaction
            finaliseTx(txBuilder)

            return Messages.SUCCESS
        }


        // Replace Responder's definition with:
        @InitiatedBy(NewHealthcareVersionContractFlow.UpdateHealthcareContractVersion::class)
        class IssueResponder(private val otherPartySession: FlowSession) : FlowLogic<Unit>() {
            @Suspendable
            override fun call() {
                subFlow(ReceiveFinalityFlow(otherPartySession))
            }
        }


    }
}


