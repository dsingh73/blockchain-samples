package com.debut.flows

import com.debut.states.Healthcarecontract.AddObservationInContractModel
import com.debut.states.Healthcarecontract.AdverseEvent
import com.debut.states.Healthcarecontract.PricingRange
import com.debut.states.treatment.SecondaryProduct
import com.google.common.reflect.TypeToken
import com.google.gson.Gson


object TypesConverter {

    fun fromStringToArrayList(value: String): List<String> {
        return try {
            val listType = object : TypeToken<ArrayList<String>>() {}.type
            Gson().fromJson(value, listType)
        } catch (ex: Exception) {
            ArrayList()
        }
    }

    fun <T> fromArrayListToString(list: List<T>): String {
        val gson = Gson()
        return gson.toJson(list)
    }



    fun fromStringToIntArrayList(value: String): List<Int> {

        return try {
            val listType = object : TypeToken<List<Int>>() {}.type
            Gson().fromJson(value, listType)
        } catch (ex: Exception) {
            ArrayList()
        }

    }


    fun pricingRangeFromStringToArraylist(value: String): List<PricingRange> {
        return try {
            val listType = object : TypeToken<List<PricingRange>>() {}.type
            Gson().fromJson(value, listType)
        } catch (ex: Exception) {
            ArrayList()
        }

    }


    fun adverseEventFromStringToArraylist(value: String): List<AdverseEvent> {
        return try {
            val listType = object : TypeToken<List<AdverseEvent>>() {}.type
            Gson().fromJson(value, listType)
        } catch (ex: Exception) {
            ArrayList()
        }

    }

    fun secondaryProductFromStringToArraylist(value: String): List<SecondaryProduct> {
        return try {
            val listType = object : TypeToken<List<SecondaryProduct>>() {}.type
            Gson().fromJson(value, listType)
        } catch (ex: Exception) {
            ArrayList()
        }

    }

    fun professionalObservationFromStringToArraylist(value: String): List<AddObservationInContractModel> {
        return try {
            val listType = object : TypeToken<List<AddObservationInContractModel>>() {}.type
            Gson().fromJson(value, listType)
        } catch (ex: Exception) {
            ArrayList()
        }

    }

}