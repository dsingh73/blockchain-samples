package com.debut.flows.Healthcarecontract

import co.paralleluniverse.fibers.Suspendable
import com.debut.clientmodel.Healthcarecontract.HealthcareContractResponseModel
import com.debut.clientmodel.Healthcarecontract.HealthcareContractResponseModelTwo
import com.debut.clientmodel.Healthcarecontract.HealthcarehramContractModel
import com.debut.clientmodel.outcomes.UpdateOutcomesModel
import com.debut.flows.BaseFlow
import com.debut.services.CordaDatabaseService
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.utilities.ProgressTracker

object AllContractFlow {
    @InitiatingFlow
    @StartableByRPC
    class Contract(
            private val pageNumber: Int,
            val userId: String
    ) : BaseFlow<Pair<Int, List<HealthcareContractResponseModelTwo>>>() {

        override val progressTracker = ProgressTracker()
        @Suspendable
        override fun call(): Pair<Int, List<HealthcareContractResponseModelTwo>> {
            val customVaultQueryService = serviceHub.cordaService(CordaDatabaseService.Service::class.java)
            return customVaultQueryService.getAllContract(pageNumber, userId)
        }
    }
}