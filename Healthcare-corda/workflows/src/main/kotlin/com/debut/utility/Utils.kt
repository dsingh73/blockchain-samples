package com.debut.utility

import net.corda.core.utilities.OpaqueBytes
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.Month
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.collections.ArrayList


object Utils {

    val PAGE_LIMIT = 15
    val EVENT_SCHEDULE_TIME_IN_DAYS = 3L
    val INTEREST_DAYS_DIFF = 30
    val DATEFORMAT = "yyyy-MM-dd"
    val DEFAULT_TIME = ""

    fun getCurrentDateTime(): String {
        val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss a")
        val currentDate = sdf.format(Date())
        print(" C DATE is  " + currentDate)
        return currentDate
    }

    fun getPartyRef(id: String): OpaqueBytes {
        val charset = Charsets.UTF_8
        val byteArray = id.toByteArray(charset)
        return OpaqueBytes(byteArray)
    }


//    fun getCategoryName(category: Int): String {
//        return when (category) {
//            1 -> {
//                "Watches"
//            }
//            2 -> {
//                "Diamonds"
//            }
//            3 -> {
//                "Cars"
//            }
//            4 -> {
//                "Gold"
//            }
//            5 -> {
//                "Fine Arts"
//            }
//            6 -> {
//                "Wine"
//            }
//            7 -> {
//                "Collectables"
//            }
//            8 -> {
//                "Coins"
//            }
//            9 -> {
//                "Antiques"
//            }
//            else -> {
//                "Uknown"
//            }
//
//        }
//    }

    fun getMonthName(month: Int): String {

        if (month > 12) {
            return "UNKNOW MONTH"
        }
        val exemple = Month.of(month)
        return exemple.toString()
    }

    fun nextScheduleTime(days: Long): Instant {
        //calculate next schedule time
        return Instant.now().plus(days, ChronoUnit.MINUTES) //next schedule time for interest

    }

    fun formateDate(date: Date): Instant {

        val ins = Instant.ofEpochMilli(date.time)
        val odt = ins.atOffset(ZoneOffset.UTC)
        return Instant.from(odt) //return utc

    }

    fun formatAmount(input: Double): String {
        val df2 = DecimalFormat("#.##")
        df2.roundingMode = RoundingMode.UP
        return df2.format(input)

    }


    fun getWeekStartDate(): String {
        val calendar = Calendar.getInstance()
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.add(Calendar.DATE, -1)
        }

        val formatter = SimpleDateFormat(DATEFORMAT)
        return formatter.format(calendar.timeInMillis) + "$DEFAULT_TIME"

    }

    fun getWeekStartDay(): Int {
        val calendar = Calendar.getInstance()
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.add(Calendar.DATE, -1)
        }

        return calendar.get(Calendar.DAY_OF_MONTH)

    }


    fun getWeekEndDate(): String {
        val calendar = Calendar.getInstance()
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.add(Calendar.DATE, 1)
        }
        calendar.add(Calendar.DATE, 6)
        val formatter = SimpleDateFormat(DATEFORMAT)
        return formatter.format(calendar.timeInMillis) + "$DEFAULT_TIME"
    }

    @Throws(Exception::class)
    fun getMonthFirstDay(): String {
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        val time = calendar.time
        val simpleDateFormat = SimpleDateFormat(DATEFORMAT)
        return simpleDateFormat.format(time) + "$DEFAULT_TIME"
    }

    @Throws(Exception::class)
    fun getMOnthLastDay(): String {
        val calendar = Calendar.getInstance()
        calendar.time = Date()
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
        val time = calendar.time
        val simpleDateFormat = SimpleDateFormat(DATEFORMAT)
        return simpleDateFormat.format(time) + "$DEFAULT_TIME"
    }


    @Throws(Exception::class)
    fun getYearFirstDay(): String {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, -(calendar.get(Calendar.MONTH)))
        calendar.set(Calendar.DATE, 1)
        val sdf = SimpleDateFormat(DATEFORMAT)
        return sdf.format(calendar.time) + "$DEFAULT_TIME"
    }

    @Throws(Exception::class)
    fun getYearLastDay(): String {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, (11 - (calendar.get(Calendar.MONTH))))
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
        val sdf = SimpleDateFormat(DATEFORMAT)
        return sdf.format(calendar.time) + "$DEFAULT_TIME"
    }

    fun getCurrentMonth(): Int {
        //get current month
        val calendar = Calendar.getInstance()
        return calendar.get(Calendar.MONTH) + 1
    }

    fun getMonth(instant: Instant): Int {
        val date = Date(instant.toEpochMilli())
        val calendar = Calendar.getInstance()
        calendar.time = date
        return calendar.get(Calendar.MONTH) + 1
    }

    fun getPreviousAndCurrentWeekDates(): List<Pair<String, String>> {

        //current week start  date
        val calendarCurrentWeek = Calendar.getInstance()
        val previousCurrentWeek = Calendar.getInstance()
        while (calendarCurrentWeek.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendarCurrentWeek.add(Calendar.DATE, -1)
            previousCurrentWeek.add(Calendar.DATE, -1)
        }

        val formatter = SimpleDateFormat(DATEFORMAT)

        val currentWeekDateOnMonday = formatter.format(calendarCurrentWeek.timeInMillis)
        //add 6 days to
        calendarCurrentWeek.add(Calendar.DATE, 6)
        val currentWeekDateOnSunday = formatter.format(calendarCurrentWeek.timeInMillis)
        val currentWeekPair = Pair(currentWeekDateOnMonday, currentWeekDateOnSunday)


        //get previous week start day date
        previousCurrentWeek.add(Calendar.DATE, -1)
        while (previousCurrentWeek.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            previousCurrentWeek.add(Calendar.DATE, -1)
        }
        val previousWeekDateOnMonday = formatter.format(previousCurrentWeek.timeInMillis)
        //add 6 days to
        calendarCurrentWeek.add(Calendar.DATE, 6)
        val previousWeekDateOnSunday = formatter.format(previousCurrentWeek.timeInMillis)
        val previousWeekPair = Pair(previousWeekDateOnMonday, previousWeekDateOnSunday)

        val dataList = ArrayList<Pair<String, String>>()
        dataList.add(currentWeekPair)
        dataList.add(previousWeekPair)

        return dataList

    }

}