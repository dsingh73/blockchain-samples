package com.debut.utility

//combine response messages
class Messages {
    companion object {

        val PROFILE_UPDATED = "user profile updated successfully"
        val PROFILE_DELETED= "user profile deleted successfully"
        val TREATMENT_UPDATED_SUCCESSFULLY= "Treatment updated successfully"
        val CONTRACT_UPDATED= "contract updated successfully"
        val SUCCESS = "Success"









    }
}


class ErrorMessage {
    companion object {
        //error or exception messages

        val NO_USER_FOUND = "No user found with the given id."

        val DATE_FORMAT_ISSUE = "Can't process date formatting"
        val HOLDING_DATE_FOR_INVESTOR = "Can't generate holding date for investor"
        val NO_AVAILABLE_NOTARY = "No available notary."


    }
}