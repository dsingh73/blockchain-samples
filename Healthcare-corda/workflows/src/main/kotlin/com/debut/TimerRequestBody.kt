package com.debut

import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class TimerRequestBody(
    val time: Long

)