package com.debut.services

import com.debut.clientmodel.Healthcarecontract.*
import com.debut.clientmodel.disease.DiseaseResponseModel
import com.debut.clientmodel.outcomes.UpdateOutcomesModel
import com.debut.clientmodel.pricing.PricingModel
import com.debut.clientmodel.treatment.TreatmentUpdateModel
import com.debut.flows.BaseFlow
import com.debut.flows.TypesConverter
import com.debut.states.Healthcarecontract.Periodicity
import com.debut.states.treatment.TreatmentProduct
import com.debut.utility.Utils
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.CordaService
import net.corda.core.serialization.SingletonSerializeAsToken
import net.corda.core.utilities.contextLogger


object CordaDatabaseService {


    @CordaService
    class Service(val services: AppServiceHub) : SingletonSerializeAsToken() {
        private companion object {
            private val log = contextLogger()

        }


        /**
         ****************** get list of diseases based on predictive search ****************
         */
        fun getListOfDiseases(disease: String): List<DiseaseResponseModel> {


            val rawQuery =
                    """
                select 
                    dis.*
                            
                 from
          vault_states vaultschema
                   join 
                        disease dis
                               
                where
                   vaultschema.output_index=dis.output_index
                    and vaultschema.transaction_id=dis.transaction_id
                    and vaultschema.state_status=0
                    and upper(dis.disease_name) like upper('$disease%')
             order by dis.created_at desc
                    
                 """


            val session = services.jdbcSession()
            return session.prepareStatement(rawQuery).use { prepStatement ->
                prepStatement.executeQuery().use { rs ->
                    val itemLists: MutableList<DiseaseResponseModel> = mutableListOf()
                    while (rs.next()) {


                        val diseaseResponseModel = DiseaseResponseModel(
                                rs.getString("disease_id"),
                                rs.getString("coding_system"),
                                rs.getString("code"),
                                rs.getString("disease_name"),
                                rs.getString("disease_desc")
                        )

                        itemLists.add(diseaseResponseModel)
                    }
                    itemLists
                }
            }
        }


        fun getAllOutComes(pageNumber: Int): Pair<Int, List<UpdateOutcomesModel>> {
            val skipLimit = (pageNumber - 1) * Utils.PAGE_LIMIT
            val nativeQuery = """
                select
                    outcomeSchema.*,(select
                    count(outcomeSchema.name)
                from
                    vault_states vaultSchema
                join
                    outcome outcomeSchema
                where
                    vaultSchema.output_index=outcomeSchema.output_index
                    and vaultSchema.transaction_id=outcomeSchema.transaction_id
                    and vaultSchema.state_status=0) as total
                from
                    vault_states vaultSchema
                join
                    outcome outcomeSchema
                where
                    vaultSchema.output_index=outcomeSchema.output_index
                    and vaultSchema.transaction_id=outcomeSchema.transaction_id
                    and vaultSchema.state_status=0
                    order by outcomeSchema.createdat desc
                    limit ${Utils.PAGE_LIMIT} offset $skipLimit
                      
                    """
            val session = services.jdbcSession()

            return session.prepareStatement(nativeQuery).use { prepStatement ->
                prepStatement.executeQuery().use { rs ->
                    val outComelist: MutableList<UpdateOutcomesModel> = mutableListOf()
                    var totalItemsCount = 0

                    while (rs.next()) {

                        totalItemsCount = rs.getInt("total")


                        val outCome = UpdateOutcomesModel(
                                rs.getString("id"),
                                rs.getString("name"),
                                TypesConverter.fromStringToIntArrayList(rs.getString("level")),
                                rs.getString("outcomeNature"),
                                rs.getInt("value"),
                                rs.getString("measurementDefination"),
                                rs.getString("calculationFormula"),
                                rs.getString("fhirAttributeSystem"),
                                rs.getString("fhirAttributeCode"),
                                rs.getFloat("specificationLimitsFrom"),
                                rs.getFloat("specificationLimitsTo"),
                                rs.getString("uom"),
                                rs.getString("reportingSource"),
                                rs.getString("responseOptions"),
                                rs.getString("notes")
                        )
                        // log.info("$assetName ")
                        outComelist.add(outCome)
                    }


                    Pair(totalItemsCount, outComelist)
                }
            }

        }


        fun getAllTreatments(pageNumber: Int): Pair<Int, List<TreatmentUpdateModel>> {
            val skipLimit = (pageNumber - 1) * Utils.PAGE_LIMIT
            val nativeQuery = """
                select
                    treatment.*,(select
                    count(treatment.disease_id)
                from
                    vault_states vaultSchema
                join
                    Treatment treatment
                where
                    vaultSchema.output_index=treatment.output_index
                    and vaultSchema.transaction_id=treatment.transaction_id
                    and vaultSchema.state_status=0) as total
                from
                    vault_states vaultSchema
                join
                    Treatment treatment
                where
                    vaultSchema.output_index=treatment.output_index
                    and vaultSchema.transaction_id=treatment.transaction_id
                    and vaultSchema.state_status=0
                    order by treatment.created_at desc
                    limit ${Utils.PAGE_LIMIT} offset $skipLimit
                      
                    """
            val session = services.jdbcSession()

            return session.prepareStatement(nativeQuery).use { prepStatement ->
                prepStatement.executeQuery().use { rs ->
                    val treamentList: MutableList<TreatmentUpdateModel> = mutableListOf()
                    var totalItemsCount = 0

                    while (rs.next()) {

                        totalItemsCount = rs.getInt("total")


                        val primaryTreatment = TreatmentProduct(
                                rs.getString("name"),
                                rs.getFloat("strength"),
                                rs.getString("strengthUom"),
                                rs.getString("dosageForm"),
                                rs.getString("serialnumber")
                        )

                        val outCome = TreatmentUpdateModel(
                                rs.getString("treatment_id"),
                                rs.getString("disease_id"),
                                rs.getString("treatment_name"),
                                rs.getString("treatment_desc"),
                                primaryTreatment,
                                TypesConverter.secondaryProductFromStringToArraylist(rs.getString("secondary_product"))
                        )
                        // log.info("$assetName ")
                        treamentList.add(outCome)
                    }

                    Pair(totalItemsCount, treamentList)
                }
            }

        }

        fun getAllContract(pageNumber: Int, userId: String): Pair<Int, List<HealthcareContractResponseModelTwo>> {
            val skipLimit = (pageNumber - 1) * Utils.PAGE_LIMIT
            val nativeQuery = """
                select
                   contractSchema.*,
                     (select user_name from users where userid=contractSchema.manufacturerid ) as manufacturerName,
                     (select user_name from users where userid=contractSchema.createdby ) as createdByName,
                     (select user_name from users where userid=contractSchema.payerId ) as payerName,
                        
                     (select
                    count(contractSchema.contractName)
                 from
                    vault_states vaultSchema
                 join
                    contract contractSchema
                 where
                    vaultSchema.output_index=contractSchema.output_index
                    and vaultSchema.transaction_id=contractSchema.transaction_id
                    and vaultSchema.state_status=0) as total
            
                 from
                    vault_states vaultSchema
                 join
                    contract contractSchema
                 where
                    vaultSchema.output_index=contractSchema.output_index
                    and vaultSchema.transaction_id=contractSchema.transaction_id
                    and vaultSchema.state_status=0
                    and (contractSchema.payerId='$userId' or contractSchema.createdby='$userId')
                    group by contractSchema.transaction_id
                    order by contractSchema.contractUpdatedAt desc
                    limit ${Utils.PAGE_LIMIT} offset $skipLimit
                    """
            val session = services.jdbcSession()
            var totalItemsCount = 0
            val outComeList: MutableList<HealthcareContractResponseModelTwo> = mutableListOf()

            val responseData = session.prepareStatement(nativeQuery).use { prepStatement ->
                prepStatement.executeQuery().use { rs ->

                    while (rs.next()) {

                        totalItemsCount = rs.getInt("total")
                        val population = ReqPopulation(
                                rs.getString("shortDesc"),
                                rs.getInt("gender"),
                                rs.getString("genderSpecify"),
                                rs.getString("country"),
                                TypesConverter.fromStringToArrayList(rs.getString("regions")),
                                rs.getInt("ageFrom"),
                                rs.getInt("ageTo")
                        )
                        val periodicity = Periodicity(
                                rs.getString("periodicityDescription"),
                                rs.getInt("periodvalue"),
                                rs.getInt("periodUom"),
                                rs.getInt("waitingPeriodValue"),
                                rs.getInt("waitingPeriodUom")

                        )

                        val validity = ReqValidity(
                                rs.getString("contractStartDate"),
                                TypesConverter.fromStringToArrayList(rs.getString("validityType")),
                                rs.getString("validFrom"),
                                rs.getString("validTo"),
                                rs.getInt("budgetAmount"),
                                rs.getInt("numberOfCourses"),
                                rs.getInt("treatmentVolume"),
                                rs.getString("budgetImpact"),
                                rs.getString("period"),
                                rs.getInt("frequency")
                        )

                        val pricing = PricingModel(
                                rs.getString("pricingDescription"),
                                rs.getInt("basePrice"),
                                rs.getString("currency"),
                                TypesConverter.pricingRangeFromStringToArraylist(rs.getString("rangeList"))
                        )

                        val aggregationLevel = ReqAggregationLevel(
                                rs.getString("aggDescription"),
                                rs.getInt("aggGender"),
                                rs.getString("aggGenderSpecify"),
                                rs.getInt("aggAgeFrom"),
                                rs.getInt("aggAgeTo"),
                                rs.getString("aggCountry"),
                                TypesConverter.fromStringToArrayList(rs.getString("aggregions")),
                                rs.getFloat("bmiFrom"),
                                rs.getFloat("bmiTo"),
                                rs.getInt("numberOfEvents"),
                                rs.getInt("numberOfPrescriptions"),
                                rs.getBoolean("patientByPatient")
                        )

//                        val adverseEvent = AdverseEvent(
//                                rs.getString("shortDesc"),
//                                rs.getString("severity"),
//                                rs.getInt("notification")
//                        )

                        val payerDataList = ArrayList<String>()
                        if (rs.getInt("contractStatus") == BaseFlow.CONTRACTSTATUS.DRAFT.value) {
                            payerDataList.addAll(TypesConverter.fromStringToArrayList(rs.getString("payerId")))

                        } else {
                            payerDataList.add(rs.getString("payerId"))
                        }
                        //add payer name as list
                        val payersNameList = ArrayList<String?>()
                        payersNameList.add(rs.getString("payerName"))

                        val outCome = HealthcareContractResponseModelTwo(
                                rs.getString("contractId"),
                                rs.getString("contractType"),
                                rs.getString("contractName"),
                                rs.getString("contractDescription"),
                                payerDataList,
                                rs.getInt("contractVersion"),
                                TypesConverter.fromStringToArrayList(rs.getString("contractNote")),
                                rs.getInt("contractStatus"),
                                rs.getString("createdBy"),
                                rs.getString("manufacturerId"),
                                validity,
                                rs.getString("treatmentId"),
                                rs.getString("outComeId"),
                                pricing,
                                aggregationLevel,
                                population,
                                periodicity,
                                TypesConverter.adverseEventFromStringToArraylist(rs.getString("adverseEvent")),
                                rs.getString("manufacturerName"),
                                rs.getString("createdByName"),
                                (rs.getString("createdBy") == userId),
                                payersNameList,
                                null
                        )
                        // log.info("$assetName ")

                        //if it's draft then get all user name list
                        if (outCome.contractStatus == BaseFlow.CONTRACTSTATUS.DRAFT.value) {
                            val ps = session.prepareStatement(getQuery(outCome.payerId))

                            for (i in outCome.payerId.indices) {
                                ps.setString(i + 1, outCome.payerId[i])
                            }

                            ps.use { prepStatement ->
                                prepStatement.executeQuery().use { rs ->
                                    val payersNameList = ArrayList<String?>()

                                    while (rs.next()) {
                                        payersNameList.add(rs.getString("user_name"))
                                    }
                                    outCome.payerName = payersNameList

                                }

                            }
                        }
                        outComeList.add(outCome)


                    }

                }
                outComeList
            }
            return  Pair(totalItemsCount, outComeList)
        }

        fun getAllHealthCareContracts(pageNumber: Int, userId: String): Pair<Int, List<HealthcareContractResponseModel>> {
            val skipLimit = (pageNumber - 1) * Utils.PAGE_LIMIT
            val nativeQuery = """
                select
                    contractSchema.*,
                       (select user_name from users where userid=contractSchema.manufacturerid ) as manufacturerName,
                        (select user_name from users where userid=contractSchema.createdby ) as createdByName,
                        (select user_name from users where userid=contractSchema.payerId ) as payerName,
                        
                        (select
                    count(contractSchema.contractName)
                from
                    vault_states vaultSchema
                join
                   contract contractSchema
                where
                    vaultSchema.output_index=contractSchema.output_index
                    and vaultSchema.transaction_id=contractSchema.transaction_id
                    and vaultSchema.state_status=0) as total
            
                from
                    vault_states vaultSchema
                join
                    contract contractSchema
                where
                    vaultSchema.output_index=contractSchema.output_index
                    and vaultSchema.transaction_id=contractSchema.transaction_id
                    and vaultSchema.state_status=0
                    and contractSchema.healthCareProfessionalId='$userId'
                    group by contractSchema.transaction_id
                    order by contractSchema.contractUpdatedAt desc
                    limit ${Utils.PAGE_LIMIT} offset $skipLimit
                       """
            val session = services.jdbcSession()

            return session.prepareStatement(nativeQuery).use { prepStatement ->
                prepStatement.executeQuery().use { rs ->
                    val outComeList: MutableList<HealthcareContractResponseModel> = mutableListOf()
                    var totalItemsCount = 0
                    while (rs.next()) {

                        totalItemsCount = rs.getInt("total")
                        val population = ReqPopulation(
                                rs.getString("shortDesc"),
                                rs.getInt("gender"),
                                rs.getString("genderSpecify"),
                                rs.getString("country"),
                                TypesConverter.fromStringToArrayList(rs.getString("regions")),
                                rs.getInt("ageFrom"),
                                rs.getInt("ageTo")
                        )
                        val periodicity = Periodicity(
                                rs.getString("periodicityDescription"),
                                rs.getInt("periodvalue"),
                                rs.getInt("periodUom"),
                                rs.getInt("waitingPeriodValue"),
                                rs.getInt("waitingPeriodUom")

                        )

                        val validity = ReqValidity(
                                rs.getString("contractStartDate"),
                                TypesConverter.fromStringToArrayList(rs.getString("validityType")),
                                rs.getString("validFrom"),
                                rs.getString("validTo"),
                                rs.getInt("budgetAmount"),
                                rs.getInt("numberOfCourses"),
                                rs.getInt("treatmentVolume"),
                                rs.getString("budgetImpact"),
                                rs.getString("period"),
                                rs.getInt("frequency")
                        )

                        val pricing = PricingModel(
                                rs.getString("pricingDescription"),
                                rs.getInt("basePrice"),
                                rs.getString("currency"),
                                TypesConverter.pricingRangeFromStringToArraylist(rs.getString("rangeList"))
                        )

                        val aggregationLevel = ReqAggregationLevel(
                                rs.getString("aggDescription"),
                                rs.getInt("aggGender"),
                                rs.getString("aggGenderSpecify"),
                                rs.getInt("aggAgeFrom"),
                                rs.getInt("aggAgeTo"),
                                rs.getString("aggCountry"),
                                TypesConverter.fromStringToArrayList(rs.getString("aggregions")),
                                rs.getFloat("bmiFrom"),
                                rs.getFloat("bmiTo"),
                                rs.getInt("numberOfEvents"),
                                rs.getInt("numberOfPrescriptions"),
                                rs.getBoolean("patientByPatient")
                        )

//                        val adverseEvent = AdverseEvent(
//                                rs.getString("shortDesc"),
//                                rs.getString("severity"),
//                                rs.getInt("notification")
//                        )

                        val outCome = HealthcareContractResponseModel(
                                rs.getString("contractId"),
                                rs.getString("contractType"),
                                rs.getString("contractName"),
                                rs.getString("contractDescription"),
                                rs.getString("payerId"),
                                rs.getInt("contractVersion"),
                                TypesConverter.fromStringToArrayList(rs.getString("contractNote")),
                                rs.getInt("contractStatus"),
                                rs.getString("createdBy"),
                                rs.getString("manufacturerId"),
                                validity,
                                rs.getString("treatmentId"),
                                rs.getString("outComeId"),
                                pricing,
                                aggregationLevel,
                                population,
                                periodicity,
                                TypesConverter.adverseEventFromStringToArraylist(rs.getString("adverseEvent")),
                                rs.getString("manufacturerName"),
                                rs.getString("createdByName"),
                                (rs.getString("createdBy") == userId),
                                rs.getString("payerName"),
                                rs.getString("healthCareProfessionalId")
                        )
                        // log.info("$assetName ")
                        outComeList.add(outCome)
                    }
                    Pair(totalItemsCount, outComeList)
                }
            }

        }

        fun getContract(contractId: String, userId: String): HealthcareaContractDetailsResponseModel? {

            val nativeQuery = """
                select
                contractSchema.*,treatment.*,outcome.*,
                (select user_name from users where userid=contractSchema.manufacturerid ) as manufacturerName,
                (select user_name from users where userid=contractSchema.createdby ) as createdByName,
                (select user_name from users where userid=contractSchema.payerId ) as payerName,
                (select user_name from users where userid=contractSchema.healthCareProfessionalId ) as healthCareProfessionalName
                from
                    vault_states vaultSchema
                left outer join
                    contract contractSchema
             
              left outer join
               (SELECT  treat.*  from    vault_states vaultschema join treatment treat 
                   where
                     vaultschema.output_index=treat.output_index
                     and vaultschema.transaction_id=treat.transaction_id
                     and vaultschema.state_status=0
                            
                ) 
                treatment
                on treatment.treatment_id=contractSchema.treatmentid 

               left outer join
               (SELECT  outcome.*  from    vault_states vaultschema join outcome outcome 
                   where
                     vaultschema.output_index=outcome.output_index
                     and vaultschema.transaction_id=outcome.transaction_id
                     and vaultschema.state_status=0
                            
                ) 
                outcome
                on outcome.id=contractSchema.outcomeid 
               
                where
                    vaultSchema.output_index=contractSchema.output_index
                    and vaultSchema.transaction_id=contractSchema.transaction_id
                    and vaultSchema.state_status=0
                    and contractSchema.contractid='$contractId'
                               
                   """
            val session = services.jdbcSession()

            val responseData = session.prepareStatement(nativeQuery).use { prepStatement ->
                prepStatement.executeQuery().use { rs ->
                    var responseData: HealthcareaContractDetailsResponseModel? = null
                    while (rs.next()) {

                        val population = ReqPopulation(
                                rs.getString("shortDesc"),
                                if (rs.getInt("gender") == 0) null else rs.getInt("gender"),
                                rs.getString("genderSpecify"),
                                rs.getString("country"),
                                TypesConverter.fromStringToArrayList(rs.getString("regions")),
                                if (rs.getInt("ageFrom") == 0) null else rs.getInt("ageFrom"),
                                if (rs.getInt("ageTo") == 0) null else rs.getInt("ageTo")
                        )
                        val periodicity = Periodicity(
                                rs.getString("periodicityDescription"),
                                if (rs.getInt("periodvalue") == 0) null else rs.getInt("periodvalue"),
                                if (rs.getInt("periodUom") == 0) null else rs.getInt("periodUom"),
                                if (rs.getInt("waitingPeriodValue") == 0) null else rs.getInt("waitingPeriodValue"),
                                if (rs.getInt("waitingPeriodUom") == 0) null else rs.getInt("waitingPeriodUom")

                        )

                        val validity = ReqValidity(
                                rs.getString("contractStartDate"),
                                TypesConverter.fromStringToArrayList(rs.getString("validityType")),
                                rs.getString("validFrom"),
                                rs.getString("validTo"),
                                if (rs.getInt("budgetAmount") == 0) null else rs.getInt("budgetAmount"),
                                if (rs.getInt("numberOfCourses") == 0) null else rs.getInt("numberOfCourses"),
                                if (rs.getInt("treatmentVolume") == 0) null else rs.getInt("treatmentVolume"),
                                rs.getString("budgetImpact"),
                                rs.getString("period"),
                                if (rs.getInt("frequency") == 0) null else rs.getInt("frequency")
                        )

                        val pricing = PricingModel(
                                rs.getString("pricingDescription"),
                                if (rs.getInt("basePrice") == 0) null else rs.getInt("basePrice"),
                                rs.getString("currency"),
                                TypesConverter.pricingRangeFromStringToArraylist(rs.getString("rangeList"))
                        )

                        val aggregationLevel = ReqAggregationLevel(
                                rs.getString("aggDescription"),
                                if (rs.getInt("aggGender") == 0) null else rs.getInt("aggGender"),
                                rs.getString("aggGenderSpecify"),
                                if (rs.getInt("aggAgeFrom") == 0) null else rs.getInt("aggAgeFrom"),
                                if (rs.getInt("aggAgeTo") == 0) null else rs.getInt("aggAgeTo"),
                                rs.getString("aggCountry"),
                                TypesConverter.fromStringToArrayList(rs.getString("aggregions")),
                                if (rs.getFloat("bmiFrom") == 0.0f) null else rs.getFloat("bmiFrom"),
                                if (rs.getFloat("bmiTo") == 0.0f) null else rs.getFloat("bmiTo"),
                                if (rs.getInt("numberOfEvents") == 0) null else rs.getInt("numberOfEvents"),
                                if (rs.getInt("numberOfPrescriptions") == 0) null else rs.getInt("numberOfPrescriptions"),
                                rs.getBoolean("patientByPatient")
                        )

//                        val adverseEvent = AdverseEvent(
//                                rs.getString("shortDesc"),
//                                rs.getString("severity"),
//                                rs.getInt("notification")
//                        )


                        val treatmentProduct = TreatmentProduct(
                                rs.getString("name"),
                                if (rs.getFloat("strength") == 0.0f) null else rs.getFloat("strength"),
                                rs.getString("strengthUom"),
                                rs.getString("dosageForm"),
                                rs.getString("serialnumber")

                        )


                        var treatmentModel: TreatmentUpdateModel? = null
                        try {
                            treatmentModel = TreatmentUpdateModel(
                                    rs.getString("treatment_id"),
                                    rs.getString("disease_id"),
                                    rs.getString("treatment_name"),
                                    rs.getString("treatment_desc"),
                                    treatmentProduct,
                                    TypesConverter.secondaryProductFromStringToArraylist(rs.getString("secondary_product"))

                            )
                        } catch (ex: Exception) {

                        }
                        var outcomesModel: UpdateOutcomesModel? = null
                        try {

                            outcomesModel = UpdateOutcomesModel(
                                    rs.getString("id"),
                                    rs.getString("name"),
                                    TypesConverter.fromStringToIntArrayList(rs.getString("level")),
                                    rs.getString("outcomeNature"),
                                    rs.getInt("value"),
                                    rs.getString("measurementDefination"),
                                    rs.getString("calculationFormula"),
                                    rs.getString("fhirAttributeSystem"),
                                    rs.getString("fhirAttributeCode"),
                                    rs.getFloat("specificationLimitsFrom"),
                                    rs.getFloat("specificationLimitsTo"),
                                    rs.getString("uom"),
                                    rs.getString("reportingSource"),
                                    rs.getString("responseOptions"),
                                    rs.getString("notes")

                            )
                        } catch (ex: Exception) {

                        }

                        val payerDataList = ArrayList<String>()
                        if (rs.getInt("contractStatus") == BaseFlow.CONTRACTSTATUS.DRAFT.value) {
                            payerDataList.addAll(TypesConverter.fromStringToArrayList(rs.getString("payerId")))

                        } else {
                            payerDataList.add(rs.getString("payerId"))
                        }
                        //add payer name as list
                        val payersNameList = ArrayList<String?>()
                        payersNameList.add(rs.getString("payerName"))

                        responseData = HealthcareaContractDetailsResponseModel(
                                rs.getString("contractId"),
                                rs.getString("contractType"),
                                rs.getString("contractName"),
                                rs.getString("contractDescription"),
                                payerDataList,
                                rs.getInt("contractVersion"),
                                TypesConverter.fromStringToArrayList(rs.getString("contractNote")),
                                TypesConverter.fromStringToArrayList(rs.getString("updateSections")),
                                rs.getInt("contractStatus"),
                                rs.getString("createdBy"),
                                rs.getString("manufacturerId"),
                                validity,
                                rs.getString("treatmentId"),
                                rs.getString("outComeId"),
                                pricing,
                                aggregationLevel,
                                population,
                                periodicity,
                                TypesConverter.adverseEventFromStringToArraylist(rs.getString("adverseEvent")),
                                treatmentModel,
                                outcomesModel,
                                rs.getString("manufacturerName"),
                                rs.getString("createdByName"),
                                (rs.getString("createdBy") == userId),
                                payersNameList,
                                TypesConverter.professionalObservationFromStringToArraylist(rs.getString("healthCareProfessionalObservationData")),
                                rs.getString("healthCareProfessionalName"),
                                rs.getString("healthCareProfessionalId")

                        )

                    }
                    responseData
                }
            }

            //if it's draft then get all user name list
            if (responseData?.contractStatus == BaseFlow.CONTRACTSTATUS.DRAFT.value) {
                val ps = session.prepareStatement(getQuery(responseData.payerId))

                for (i in responseData.payerId.indices) {
                    ps.setString(i + 1, responseData.payerId[i])
                }
                return ps.use { prepStatement ->
                    prepStatement.executeQuery().use { rs ->
                        val payersNameList = ArrayList<String?>()
                            while (rs.next()) {
                            payersNameList.add(rs.getString("user_name"))
                        }
                        responseData.payersName = payersNameList
                        responseData
                    }

                }
            }
            return responseData
        }

    }

  fun getQuery(ids:List<String?>):String
    {
        val query = StringBuilder("select user_name from users where userid in(")
        for (i in ids.indices) {
            if (i > 0) {
                query.append(",")
            }
            query.append("?")
        }
        query.append(")")

        return query.toString()
    }

}










