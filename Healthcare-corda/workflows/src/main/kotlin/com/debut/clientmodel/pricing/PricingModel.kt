package com.debut.clientmodel.pricing

import com.debut.states.Healthcarecontract.PricingRange
import net.corda.core.serialization.CordaSerializable
import java.time.Instant

@CordaSerializable
data class PricingModel(
        val description: String?,
        val basePrice: Int?,
        val currency: String?,
        val rangeList: List<PricingRange>?
)