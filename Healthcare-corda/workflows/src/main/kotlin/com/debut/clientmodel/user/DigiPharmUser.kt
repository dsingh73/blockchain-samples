package com.debut.clientmodel.user

import com.debut.states.user.UserAddress
import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class HealthcareUser(
        val _id: String,
        val email: String?,
        val name: String,
        val phone: String?,
        val address: UserAddress?,
        val role: Int,
        val status:Int



)

