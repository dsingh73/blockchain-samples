package com.debut.clientmodel.aggregation

import net.corda.core.serialization.CordaSerializable
import java.time.Instant

@CordaSerializable
data class AggregationModel(
        val ageFrom: Int,
        val ageTo:Int,
        val country: String,
        val gender: Int, //1-male , 2- female
        val regions: List<String>,
        val bmiFrom: Float,
        val bmiTo: Float,
        val numberOfEvents: Int,
        val numberOfPrescriptions: Int,
        val patientByPatient: Boolean
)