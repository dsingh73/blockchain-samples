package com.debut.clientmodel.user

import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class AddMoneyToUserWallet(
        val userId: String,
        val walletAmount: Double,
        val paymentSource:Int
)

