package com.debut.clientmodel.Healthcarecontract


import com.debut.clientmodel.pricing.PricingModel
import com.debut.states.Healthcarecontract.AdverseEvent
import com.debut.states.Healthcarecontract.AggregationLevel
import com.debut.states.Healthcarecontract.Periodicity
import io.swagger.annotations.ApiModelProperty
import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class UpdateHealthcareaContractModel(

        val contractId: String,
        val contractType: String?, // 1-Volume, 2- Outcome, 3- Rebate, 4- Refund
        val contractName: String?,
        val contractDescription: String?,
        val payerId: String?, //unique payer id
        val contractVersion: Int?,
        val contractNote: List<String>?,
        val contractStatus: Int?, // PENDING, REJECTED, ACCEPTED, SIGNED_OFF, OPEN, CANCELLED, RUNNING, CLOSED
        val createdBy: String?, //user unique id
        val manufacturerId: String?, //  unique manufacturer id
        val validity:ReqValidity,
        val treatmentId: String?, //  unique treatmentId id
        val outcomeId: String?, //  unique outComeId id
        val pricing: PricingModel?, //  pricing level details
        val aggregationLevel: ReqAggregationLevel?, //  aggregation level details
        val population: ReqPopulation?,
        val periodicity: Periodicity?,
        val adverseEvent: List<AdverseEvent>?,
        val healthCareProfessional:String?

)




