package com.debut.clientmodel.graphdata

import com.debut.states.user.UserAddress
import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class DashboardDataModel(
        var activeContracts: Int=0,
        var pendingContracts: Int=0,
        var activeContractgrowthPercentage: Float=0.0f,
        var pendingContractgrowthPercentage: Float=0.0f

)

