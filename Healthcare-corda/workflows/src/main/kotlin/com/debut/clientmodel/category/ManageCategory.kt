package com.debut.clientmodel.category

import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class ManageCategory(
        val categoryName: String,
        val type: Int
)