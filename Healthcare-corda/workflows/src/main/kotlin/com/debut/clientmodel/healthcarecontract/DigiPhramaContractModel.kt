package com.debut.clientmodel.Healthcarecontract



import com.debut.clientmodel.pricing.PricingModel
import com.debut.states.Healthcarecontract.AdverseEvent
import com.debut.states.Healthcarecontract.AggregationLevel
import com.debut.states.Healthcarecontract.Periodicity
import io.swagger.annotations.ApiModelProperty
import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class HealthcarehramContractModel(


        val contractType: String?, // 1-Volume, 2- Outcome, 3- Rebate, 4- Refund
        val contractName: String?,
        val contractDescription: String?,
        val payerId: List<String>?, //payers id list
        val contractVersion: Int?,
        val contractNote: List<String>?,
        val contractStatus: Int?, // PENDING, REJECTED, ACCEPTED, SIGNED_OFF, OPEN, CANCELLED, RUNNING, CLOSED
        val createdBy: String?, //user unique id
        val manufacturerId: String?, //  unique manufacturer id
        val validity:ReqValidity,
        val treatmentId: String?, //  unique treatmentId id
        val outcomeId: String?, //  unique outComeId id
        val pricing: PricingModel?, //  pricing level details
        val aggregationLevel: ReqAggregationLevel?, //  aggregation level details
        val population: ReqPopulation?,
        val periodicity: Periodicity?,
        val adverseEvent: List<AdverseEvent>?,
        val healthCareProfessional:String?

)
@CordaSerializable
data class ReqPopulation(
        val shortDesc: String?,
        val gender: Int?, //1- male , 2- female
        var genderSpecify:String?, //other gender specification
        val country: String?,
        val regions: List<String>?,
        val ageFrom: Int?,
        val ageTo: Int?

)

@CordaSerializable
data class ReqValidity(
        val contractStartDate: String?,
        val validityType: List<String>?, //1- time based, 2- volume base, 3- Budget Impact, 4- Treatment course
        val validFrom: String?,
        val validTo:String?,
        val budgetAmount:Int?,
        val numberOfCourses:Int?,
        val treatmentVolume: Int?,
        val budgetImpact:String?,
        val period:String?,
        val frequency:Int?

)

@CordaSerializable
data class ReqAggregationLevel(
        var aggDescription:String?,
        val aggGender:Int?,
        var aggGenderSpecify:String?, //other gender specification
        val aggAgeFrom: Int?,
        val aggAgeTo: Int?,
        val aggCountry: String?,
        val aggregions:List<String>?,
        val bmiFrom: Float?,
        val bmiTo: Float?,
        val numberOfEvents: Int?,
        val numberOfPrescriptions: Int?,
        val patientByPatient: Boolean?
)


