package com.debut.clientmodel.Healthcarecontract


import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class UpdateStatusContract(
        val contractId: String,
        val status: Int,
        val note:String?,
        val updateSections:List<String>?

)

