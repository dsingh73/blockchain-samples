package com.debut.contracts


import com.debut.states.aggregation.AggregationLevelState
import com.debut.states.Healthcarecontract.HealthcareContractState
import com.debut.states.disease.DiseaseState
import com.debut.states.outcome.OutcomeState
import com.debut.states.outcomedefination.OutcomeDefinationState
import com.debut.states.treatment.TreatmentState
import com.debut.states.user.UserState
import net.corda.core.contracts.*
import net.corda.core.internal.requiredContractClassName
import net.corda.core.transactions.LedgerTransaction
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.regex.Pattern


// ************
// * Contract *
// ************
class RegistrationContract : Contract {
    companion object {
        // Used to identify our contract when building a transaction.
        const val ID = "com.debut.contracts.RegistrationContract"
        const val DATE_FORMAT = "yyyy-MM-dd"
        //pattern to validate
        val EMAIL_ADDRESS_PATTERN: Pattern = Pattern.compile(
                "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                        "\\@" +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                        "(" +
                        "\\." +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                        ")+"
        )

    }

    // A transaction is valid if the verify() function of the contract of all the transaction's input and output states
    // does not throw an exception.
    override fun verify(tx: LedgerTransaction) {


        val command = tx.commands.requireSingleCommand<HealthcareCommand>()

        when (command.value) {


            is HealthcareCommand.CreateUser -> requireThat {
                val output = tx.outputsOfType<UserState>().single()
                "Please enter user id." using (output.id.isNotBlank())
                "Please enter user name." using (output.name.isNotBlank())
               // "Please enter user valid type." using (output.role in 1..3)
            }


            is HealthcareCommand.CreateTreatment -> requireThat {
                val output = tx.outputsOfType<TreatmentState>().single()
                "Please enter treatment name." using (output.treatmentName.isNotBlank())
                "Please enter treatment description." using (output.treatmentDesc.isNotBlank())
            }

            is HealthcareCommand.CreateContract -> requireThat {
                val output = tx.outputsOfType<HealthcareContractState>().single()
                "Please enter valid contract status." using (output.contractStatus in 1..8)
            }

            is HealthcareCommand.UpdateContract -> requireThat {
                val output = tx.outputsOfType<HealthcareContractState>().single()
                "Please enter valid contract status." using (output.contractStatus in 1..8)
            }
            is HealthcareCommand.UpdateContractStatus -> requireThat {
                val output = tx.outputsOfType<HealthcareContractState>().single()
                "Please enter valid contract status." using (output.contractStatus in 1..8)
            }

            is HealthcareCommand.UpdateUser -> requireThat {
                val output = tx.outputsOfType<UserState>().single()
               // "Please enter valid email." using (output.userEmail.isNotBlank())
            }


            is HealthcareCommand.CreateDisease -> requireThat {
                val output = tx.outputsOfType<DiseaseState>().single()
                "Please provide codingsystem for disease." using (output.codingSystem.isNotBlank())
                "Please provide code for disease." using (output.code.isNotBlank())
                "Please provide disease name." using (output.diseaseName.isNotBlank())
                "Please provide disease description." using (output.diseaseDesc.isNotBlank())

            }


            is HealthcareCommand.CreateOutcome -> requireThat {
                val output = tx.outputsOfType<OutcomeState>().single()
                "Please enter name." using (output.name != null)
                "Transaction must be signed by the sender." using (tx.outputsOfType<OutcomeState>().single().partyList.single().owningKey == command.signers.single())

            }
            is HealthcareCommand.UpdateOutcome -> requireThat {
                val output = tx.outputsOfType<OutcomeState>().single()
                "Please enter name." using (!output.Id.isNullOrBlank())
                "Transaction must be signed by the sender." using (tx.outputsOfType<OutcomeState>().single().partyList.single().owningKey == command.signers.single())

            }


        }
    }


    //validate email address
    private fun isEmailValid(email: String): Boolean {
        return EMAIL_ADDRESS_PATTERN.toRegex().matches(email)
    }


}

class OutcomeStateContract

//all commands used for different flows
sealed class HealthcareCommand : TypeOnlyCommandData() {

    class CreateTreatment : HealthcareCommand()
    class UpdateTreatment : HealthcareCommand()
    class CreateDisease : HealthcareCommand()
    class CreateUser : HealthcareCommand()
    class UpdateUser : HealthcareCommand()
    class DeleteUser : HealthcareCommand()
    class Timer : HealthcareCommand()
    class CreateAggregation : HealthcareCommand()
    class CreateOutcome : HealthcareCommand()
    class UpdateOutcome : HealthcareCommand()
    class CreateContract : HealthcareCommand()
    class CreateDraft : HealthcareCommand()
    class UpdateContract : HealthcareCommand()
    class UpdateContractStatus : HealthcareCommand()
    class CancelContract : HealthcareCommand()

}




