package com.debut.states.user


import com.debut.contracts.RegistrationContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.ContractState
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable
import java.time.Instant

// *********
// * State *
// *********
@CordaSerializable
@BelongsToContract(RegistrationContract::class)
data class UserState(
        val id: String,
        val email: String?,
        val name: String,
        val phone: String?,
        val address: UserAddress?,
        val role: Int?,
        val accessKey: String?,
        var partyList: List<Party>,
        val createdAt: Instant = Instant.now(),
        val updatedAt: Instant = Instant.now()

) : ContractState, QueryableState {
    override fun generateMappedObject(schema: MappedSchema): PersistentState {
        return when (schema) {
            is UserSchema -> UserSchema.PersistentUserState(
                    this.id,
                    this.email,
                    this.name,
                    this.role,
                    this.phone,
                    this.accessKey,
                    this.address,
                    this.createdAt,
                    this.updatedAt
            )

            else -> throw IllegalArgumentException("Unrecognised schema $schema")
        }
    }

    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(UserSchema)
    override val participants get() = partyList

    fun copyNewData(
            newUserName: String,
            newUserEmail: String?,
            newUserAddress: UserAddress?,
            newUserPhone: String?): UserState {
        return copy(
                name = checkValue(newUserName, name)?:"",
                email = checkValue(newUserEmail, email),
                address = newUserAddress ?: address,
                phone = checkValue(phone, newUserPhone),
                updatedAt = Instant.now()
        )
    }


    //to verify is there any new value or not
    private fun checkValue(newValue: String?, oldValue: String?): String? {
        return if (newValue?.isNotBlank()==true) {
            newValue
        } else {
            oldValue
        }
    }


}
