package com.debut.states.user

import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class UserAddress(
        val line: String,
        val city: String,
        val state: String,
        val country: String,
        val pinCode: String

){
    constructor() : this("", "", "", "", "")
}