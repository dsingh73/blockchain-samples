package com.debut.states.user

import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import java.time.Instant
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

object Schema1
object UserAddressSchema : MappedSchema(Schema1.javaClass, 1, listOf(UserAddressSchema.PersistentUserAddressState::class.java)) {
    @Entity
    @Table(name = "address")
    class PersistentUserAddressState(
            @Column(name = "user_id")
            var userId: String,
            @Column(name = "line")
            var line: String,
            @Column(name = "city")
            var city: String,
            @Column(name = "state")
            var state: String,
            @Column(name = "country")
            var country: String,
            @Column(name = "pin_code")
            var pinCode: String,
            @Column(name = "created_at")
            var createdAt: Instant,
            @Column(name = "updated_at")
            var updatedAt: Instant

    ) : PersistentState() {
        // Default constructor required by hibernate.
        constructor() : this(
                "",
                "",
                "",
                "",
                "",
                "",
                Instant.now(),
                Instant.now()
        )
    }
}