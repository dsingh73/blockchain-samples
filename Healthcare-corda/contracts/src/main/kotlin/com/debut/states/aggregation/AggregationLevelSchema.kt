package com.debut.states.aggregation

import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table


object Schema
object AggregationLevelSchema : MappedSchema(Schema.javaClass, 1, listOf(AggregationLevelSchema.PersistentAggregationLevel::class.java)) {

    @Entity
    @Table(name = "aggregation_level")
    class PersistentAggregationLevel(
            @Column(name = "id")
            var id: String


    ) : PersistentState() {
        // Default constructor required by hibernate.
        constructor() : this("")
    }
}

