package com.debut.states.category


import com.debut.contracts.RegistrationContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.ContractState
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable
import java.time.Instant

// *********
// * State *
// *********
@CordaSerializable
@BelongsToContract(RegistrationContract::class)
data class AssetCategoryState(
        val categoryMap: Map<Int, String>,
        val updatedAt: Instant,
        var partyList: List<Party>

) : ContractState {



    override val participants get() = partyList

    fun updateCategory(newCategoryMap: Map<Int, String>): AssetCategoryState {
        return copy(
                categoryMap = newCategoryMap,
                updatedAt = Instant.now()
        )

    }

}
