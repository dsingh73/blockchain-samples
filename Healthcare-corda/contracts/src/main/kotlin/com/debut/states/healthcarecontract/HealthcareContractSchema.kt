package com.debut.states.Healthcarecontract

import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import java.time.Instant
import java.util.ArrayList
import javax.persistence.*


object Schema
object HealthcareContractSchema : MappedSchema(Schema.javaClass, 1, listOf(PersistentHealthcareContract::class.java)) {

    @Entity
    @Table(name = "contract")
    class PersistentHealthcareContract(

            @Column(name = "contractId")
            var contractId: String?,
            @Column(name = "contractType")
            var contractType: String?, // 1-Volume, 2- Outcome, 3- Rebate, 4- Refund
            @Column(name = "contractName")
            var contractName: String?,
            @Column(name = "contractDescription")
            var contractDescription: String?,
            @Column(name = "payerId")
            var payerId: String?, //unique payer id
            @Column(name = "contractVersion")
            var contractVersion: Int?,
            @Column(name = "contractNote")
            var contractNote: String?,
            @Column(name = "contractStatus")
            var contractStatus: Int?, // PENDING, REJECTED, ACCEPTED, SIGNED_OFF, OPEN, CANCELLED, RUNNING, CLOSED
            @Column(name = "createdBy")
            var createdBy: String?, //user unique id
            @Column(name = "manufacturerId")
            var manufacturerId: String?, //  unique manufacturer id
            @Embedded
            var validity: Validity?,
            @Column(name = "treatmentId")
            var treatmentId: String?, //  unique treatmentId id
            @Column(name = "outcomeId")
            var outComeId: String?, //  unique outComeId id

            @Embedded
            var pricing: Pricing?, //  pricing

            @Embedded
            var aggregationLevel: AggregationLevel?, //  unique pricingId id

            @Column(name = "contractCreatedAt")
            var contractCreatedAt: Instant?,
            @Column(name = "contractUpdatedAt")
            var contractUpdatedAt: Instant?,
            @Embedded
            var population: Population?,
            @Embedded
            var periodicity: Periodicity?,
            @Column(name = "adverseEvent")
            val adverseEvent: String?,
            @Embedded
            val contractHistory: ContractHistory,
            @Column(name = "updateSections")
            var updateSections: String?, //  update sections
            @Column(name = "healthCareProfessionalId")
            var healthCareProfessional: String?, //  update sections
            @Column(name = "healthCareProfessionalObservationData")
            var healthCareProfessionalObservationData: String? //  update sections



    ) : PersistentState() {
        // Default constructor required by hibernate.
        constructor() : this(
                "",
                "",
                "",
                "",
                "",
                0,
                "",
                0,
                "",
                "",
                Validity(),
                "",
                "",
                Pricing(),
                AggregationLevel(),
                Instant.now(),
                Instant.now(),
                Population(),
                Periodicity(),
                "",
                ContractHistory(),
                "",
                "",
                ""
        )
    }


}

