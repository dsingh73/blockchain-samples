package com.debut.states.aggregation


import com.debut.contracts.RegistrationContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.ContractState
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable
import java.time.Instant

// *********
// * State *
// *********
@CordaSerializable
@BelongsToContract(RegistrationContract::class)
data class AggregationLevelState(
        val Id: String,
        val ageFrom: Int,
        val ageTo: Int,
        val country: String,
        val gender: Int, //1-male , 2- female
        val regions: List<String>,
        val bmiFrom: Float,
        val bmiTo: Float,
        val numberOfEvents: Int,
        val numberOfPrescriptions: Int,
        val patientByPatient: Boolean,
        val createdAt: Instant,
        val updatedAt: Instant,
        var partyList: List<Party>
) : ContractState, QueryableState {
    override fun generateMappedObject(schema: MappedSchema): PersistentState {
        return when (schema) {
            is AggregationLevelSchema -> AggregationLevelSchema.PersistentAggregationLevel(
                    this.Id
            )
            else -> throw IllegalArgumentException("Unrecognised schema $schema")
        }
    }

    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(AggregationLevelSchema)
    override val participants get() = partyList


}
