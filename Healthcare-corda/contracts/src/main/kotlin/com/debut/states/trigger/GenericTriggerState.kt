package com.debut.states.trigger


import com.debut.contracts.RegistrationContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.ContractState
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable
import java.time.Instant

// *********
// * State *
// *********
@CordaSerializable
@BelongsToContract(RegistrationContract::class)
data class GenericTriggerState(
        val Id: String,
        val description: String,
        val status: Int, // OPEN,  CHALLENGED,  CONFIRMED
        var partyList: List<Party>,
        val createdAt: Instant = Instant.now(),
        val updatedAt: Instant = Instant.now()

) : ContractState {
    //    override fun generateMappedObject(schema: MappedSchema): PersistentState {
//        return when (schema) {
//            is UserSchema -> UserSchema.PersistentUserState(
//                    this.userId,
//                    this.userEmail,
//                    this.userName,
//                    this.userRole,
//                    this.userAddrese,
//                    this.userPhone,
//                    this.createdAt,
//                    this.updatedAt
//            )
//            else -> throw IllegalArgumentException("Unrecognised schema $schema")
//        }
//    }
//
//    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(UserSchema)
    override val participants get() = partyList


}
