package com.debut.states.user

import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import java.time.Instant
import javax.persistence.Column
import javax.persistence.Embedded
import javax.persistence.Entity
import javax.persistence.Table

object Schema
object UserSchema : MappedSchema(Schema.javaClass, 1, listOf(UserSchema.PersistentUserState::class.java)) {
    @Entity
    @Table(name = "users")
    class PersistentUserState(
            @Column(name = "userid")
            var userId: String,
            @Column(name = "user_email")
            var userEmail: String??,
            @Column(name = "user_name")
            var userName: String?,
            @Column(name = "user_role")
            var userRole: Int?,
            @Column(name = "user_phone")
            var userPhone: String?,
            @Column(name = "access_key")
            var accessKey: String?,
            @Embedded
            var address: UserAddress?,
            @Column(name = "created_at")
            var createdAt: Instant,
            @Column(name = "updated_at")
            var updatedAt: Instant

    ) : PersistentState() {
        // Default constructor required by hibernate.
        constructor() : this("",
                "",
                "",
                0,
                "",
                "",
                UserAddress(),
                Instant.now(),
                Instant.now()
        )
    }
}