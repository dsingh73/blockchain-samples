package com.debut.states.Healthcarecontract


import com.debut.contracts.RegistrationContract
import com.debut.states.outcome.OutcomeState
import com.google.gson.Gson
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.ContractState
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable
import java.time.Instant
import java.util.ArrayList
import javax.persistence.ElementCollection
import javax.rmi.CORBA.Util
import javax.transaction.Transactional


// *********
// * State *
// *********
@CordaSerializable
@Transactional
@BelongsToContract(RegistrationContract::class)
data class HealthcareContractState(

        val contractId: String,
        var contractType: String?, // 1-Volume, 2- Outcome, 3- Rebate, 4- Refund
        var contractName: String?,
        var contractDescription: String?,
        var payerId: String?, //unique payer id
        var contractVersion: Int?,
        var contractNote: List<String>?,
        var updateSections: List<String>?,
        var contractStatus: Int?, // PENDING, REJECTED, ACCEPTED, SIGNED_OFF, OPEN, CANCELLED, RUNNING, CLOSED
        var createdBy: String?, //user unique id
        var manufacturerId: String?, //  unique manufacturer id
        var validity: Validity,
        var treatmentId: String?, //  unique treatmentId id
        var outcomeId: String?, //  unique outComeId id
        var pricing: Pricing?, //  unique pricingId id
        var aggregationLevel: AggregationLevel?, //  aggregation level
        var population: Population?,
        var periodicity: Periodicity?,
        var adverseEvent: List<AdverseEvent>?,
        var contractHistory: ContractHistory,
        var healthCareProfessional: String?,
        val contractCreatedAt: Instant,
        var contractUpdatedAt: Instant,
        var partyList: List<Party>,
        var professionalObservationData:List<AddObservationInContractModel>?=null,
        var observationUpdatedCount:Int=0

) : ContractState, QueryableState {

    override fun generateMappedObject(schema: MappedSchema): PersistentState {
        return when (schema) {
            is HealthcareContractSchema -> {
                HealthcareContractSchema.PersistentHealthcareContract(
                        this.contractId,
                        this.contractType, // 1-Volume, 2- Outcome, 3- Rebate, 4- Refund
                        this.contractName,
                        this.contractDescription,
                        this.payerId, //unique payer id
                        this.contractVersion,
                        fromArrayListToString(this.contractNote ?: ArrayList()),
                        this.contractStatus, // PENDING, REJECTED, ACCEPTED, SIGNED_OFF, OPEN, CANCELLED, RUNNING, CLOSED
                        this.createdBy,     //user unique id
                        this.manufacturerId,  //  unique manufacturer id
                        this.validity,       //validity model
                        this.treatmentId,   //  unique treatmentId id
                        this.outcomeId,    //  unique outComeId id
                        this.pricing,     //  pricing model
                        this.aggregationLevel, // aggregation level model
                        this.contractCreatedAt,
                        this.contractUpdatedAt,
                        this.population,
                        this.periodicity,
                        fromArrayListToString(this.adverseEvent ?: ArrayList()),
                        this.contractHistory,
                        fromArrayListToString(this.updateSections?:ArrayList()),
                        this.healthCareProfessional,
                        fromArrayListToString(this.professionalObservationData?:ArrayList())

                )

            }

            else -> throw Throwable("Unrecognised schema $schema")
        }
    }

    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(HealthcareContractSchema)

    override val participants get() = partyList


    fun copyNewData(): HealthcareContractState {
        return copy()
    }


    fun copyOfDataWithNewStatus(
            status: Int,
            updatedAt: Instant,
            newContractNote: List<String>,
            newUpdateSections: List<String>?

    ): HealthcareContractState {
        return copy(
                contractStatus = status,
                contractUpdatedAt = updatedAt,
                contractNote = newContractNote,
                updateSections = newUpdateSections //update new sections

        ) //To change body of created functions use File | Settings | File Templates.
    }

    private fun <T> fromArrayListToString(list: List<T>): String {
        val gson = Gson()
        return gson.toJson(list)
    }


}


@CordaSerializable
data class Population(
        var shortDesc: String?,
        var gender: Int?, //1- male , 2- female
        var genderSpecify: String?, //other gender specification
        var country: String?,
        var regions: String?,
        var ageFrom: Int?,
        var ageTo: Int?

) {
    constructor() : this("", 0, "", "", "", 0, 0)
}


// Default constructor required by hibernate.


@CordaSerializable
data class Periodicity(
        var periodicityDescription: String?,
        var periodvalue: Int?,
        var periodUom: Int?, //DA, WE, MO, YR
        var waitingPeriodValue: Int?,
        var waitingPeriodUom: Int?  //DA, WE, MO, YR
) {
    constructor() : this("", 0, 0, 0, 0)
}

@CordaSerializable
data class Pricing(
        var pricingDescription: String?,
        var basePrice: Int?,
        var currency: String?,
        var rangeList: String?
) {
    constructor() : this("", 0, "", "")
}

@CordaSerializable
data class PricingRange(
        val from: Int?,
        val to: Int?,
        val price: Int?,
        val priceUom: String?

)

@CordaSerializable
data class AggregationLevel(
        var aggDescription: String?,
        var aggGender: Int?, //0-male ,1-female, 2-other, 3=unknown
        var aggGenderSpecify: String?, //other gender specification
        var aggAgeFrom: Int?,
        var aggAgeTo: Int?,
        var aggCountry: String?,
        var aggregions: String?,
        var bmiFrom: Float?,
        var bmiTo: Float?,
        var numberOfEvents: Int?,
        var numberOfPrescriptions: Int?,
        var patientByPatient: Boolean?
) {
    constructor() : this("", 0, "", 0, 0, "", "", 0.0f, 0.0f, 0, 0, false)
}


@CordaSerializable
data class Validity(
        val contractStartDate: String?,
        val validityType: String, // array of 1- time based, 2- volume base, 3- Budget Impact, 4- Treatment course
        val validFrom: String?,
        val validTo: String?,
        val budgetAmount: Int?,
        val numberOfCourses: Int?,
        val treatmentVolume: Int?,
        val budgetImpact: String?,
        val period: String?,
        val frequency: Int?

) {
    constructor() : this("", "", "", "", 0, 0, 0, "", "", 0)
}

@CordaSerializable
data class AdverseEvent(
        val adverseEventShortDesc: String?,
        val severity: String?,
        val notification: Int?
) {
    constructor() : this("", "", 0)
}

@CordaSerializable
data class ContractHistory(
        val versionName: String,
        val versionNumber: Int,
        val createdAt: Instant

) {
    constructor() : this("", 0, Instant.now())
}

@CordaSerializable
data class AddObservationInContractModel(
        val bloodpressure: Int?,
        val contractId: String,
        val heartbeat: Int?,
        val temperature: Int?,
        val createdAt:String?
)
{
    constructor() : this(0, "",0,0,"")
}