package com.debut.client


import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class Application {

    @Bean
    fun rpcClient(): WrapperRpcClient {
        val client = WrapperRpcClient()
        client.createClient()
        return client
    }
}

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}