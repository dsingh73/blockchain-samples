package com.debut.client.api.contract

import com.debut.client.WrapperRpcClient
import com.debut.client.common.CommonMethods
import com.debut.client.common.Messages
import com.debut.clientmodel.Healthcarecontract.HealthcarehramContractModel
import com.debut.clientmodel.Healthcarecontract.UpdateHealthcareaContractDraftModel
import com.debut.clientmodel.Healthcarecontract.UpdateHealthcareaContractModel
import com.debut.clientmodel.Healthcarecontract.UpdateStatusContract
import com.debut.flows.BaseFlow
import com.debut.flows.Healthcarecontract.*
import com.debut.responsemodel.CustomError
import com.debut.responsemodel.ItemsListResponseModel
import com.debut.responsemodel.ResponseModel
import com.debut.states.Healthcarecontract.AddObservationInContractModel
import io.swagger.annotations.Api
import net.corda.core.messaging.startFlow
import net.corda.core.utilities.getOrThrow

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@Api(value = "ContractOperations", description = "Handle Contract operations")
class HealthcareContractOperation : CommonMethods() {

    @Autowired
    lateinit var rpc: WrapperRpcClient


    @PostMapping("/create-contract")
    fun createContract(@RequestBody request: HealthcarehramContractModel): ResponseEntity<*> {

        return try {

            when {
                //check if creator exists
                checkIfUserExists(request.createdBy ?: "", rpc) -> {
                    ResponseEntity(CustomError("${Messages.CREATED_BY_NOT_FOUND}=${request.createdBy}", HttpStatus.BAD_REQUEST),
                            HttpStatus.CONFLICT)
                }
//                //check if manufacturer exists
//                checkIfUserExists(request.manufacturerId ?: "", BaseFlow.USERROLE.MANUFACTURER.value, rpc) -> {
//                    ResponseEntity(CustomError("${Messages.MANUFACTURER_NOT_FOUND}=${request.manufacturerId}", HttpStatus.BAD_REQUEST),
//                            HttpStatus.CONFLICT)
//                }
                //check if treatment already exists with given id
                getTreatment(request.treatmentId ?: "", rpc) == null -> {
                    ResponseEntity(CustomError("${Messages.NO_TREATMENT_FOUND}=${request.treatmentId}", HttpStatus.BAD_REQUEST),
                            HttpStatus.CONFLICT)
                }
                //check if outcome already exists with given id
                getOutcome(request.outcomeId ?: "", rpc) == null -> {
                    ResponseEntity(CustomError("${Messages.NO_OUTCOME_FOUND}=${request.treatmentId}", HttpStatus.BAD_REQUEST),
                            HttpStatus.CONFLICT)
                }

                else -> {
                    val flowHandle = rpc.getClient()?.startFlow(HealthcareContractFlow::AddHealthcareContract,
                            request)
                    flowHandle.use {
                        val id = it?.returnValue?.getOrThrow()
                        ResponseEntity(ResponseModel(Messages.CONTRACT_CREATED_SUCCESSFULY, HttpStatus.OK, id
                                ?: ""), HttpStatus.OK)
                    }
                }
            }
        } catch (e: Exception) {
            ResponseEntity(CustomError(formatErrorMsg(e.message), HttpStatus.CONFLICT), HttpStatus.BAD_REQUEST)
        }

    }


    @PutMapping("/update-contract")
    fun updateContract(@RequestBody request: UpdateHealthcareaContractModel): ResponseEntity<Any> {
        return try {
            val flowHandle = rpc.getClient()?.startFlow(UpdateHealthcareContractFlow::UpdateHealthcareContract,
                    request)
            flowHandle.use {
                val id = it?.returnValue?.getOrThrow()
                ResponseEntity(ResponseModel(Messages.CONTRACT_UPDATED_SUCCESSFULY, HttpStatus.OK, id
                        ?: ""), HttpStatus.OK)
            }
        } catch (e: Exception) {
            ResponseEntity(CustomError(formatErrorMsg(e.message), HttpStatus.CONFLICT), HttpStatus.BAD_REQUEST)
        }

    }

    @PutMapping("/update-professional-data")
    fun updateProfessionalData(@RequestBody request: AddObservationInContractModel): ResponseEntity<Any> {
        return try {
            val flowHandle = rpc.getClient()?.startFlow(UpdateHealthcareContractProfessionalDataFlow::UpdateHealthcareContract,
                    request)
            flowHandle.use {
                val id = it?.returnValue?.getOrThrow()
                ResponseEntity(ResponseModel(Messages.CONTRACT_PROFESSIONAL_DATA_UPDATED_SUCCESSFULY, HttpStatus.OK, id
                        ?: ""), HttpStatus.OK)
            }
        } catch (e: Exception) {
            ResponseEntity(CustomError(formatErrorMsg(e.message), HttpStatus.CONFLICT), HttpStatus.BAD_REQUEST)
        }

    }

    @PostMapping("/contract-draft")
    fun contractDraft(@RequestBody request: UpdateHealthcareaContractDraftModel): ResponseEntity<*> {


        return try {
            when {
                //check if creator exists
                (request.createdBy?.isNotEmpty() == true && checkIfUserExists(request.createdBy ?: "", rpc)) -> {
                    ResponseEntity(CustomError("${Messages.CREATED_BY_NOT_FOUND}=${request.createdBy}", HttpStatus.BAD_REQUEST),
                            HttpStatus.CONFLICT)
                }
                //check if treatment already exists with given id
                (request.treatmentId?.isNotEmpty() == true && getTreatment(request.treatmentId ?: "", rpc) == null) -> {
                    ResponseEntity(CustomError("${Messages.NO_TREATMENT_FOUND}=${request.treatmentId}", HttpStatus.BAD_REQUEST),
                            HttpStatus.CONFLICT)
                }
                //check if outcome already exists with given id
                (request.outcomeId?.isNotEmpty() == true && getOutcome(request.outcomeId ?: "", rpc) == null) -> {
                    ResponseEntity(CustomError("${Messages.NO_OUTCOME_FOUND}=${request.outcomeId}", HttpStatus.BAD_REQUEST),
                            HttpStatus.CONFLICT)
                }

                //check if contract exists or not
                (request.contractId?.isNotEmpty() == true && getContract(request.contractId ?: "", rpc) == null) -> {
                    ResponseEntity(CustomError("${Messages.NO_CONTRACT_FOUND}=${request.contractId}", HttpStatus.BAD_REQUEST),
                            HttpStatus.CONFLICT)
                }

                else -> {
                    val flowHandle = rpc.getClient()?.startFlow(UpdateHealthcareContractDraftFlow::UpdateHealthcareContractDraft,
                            request)
                    flowHandle.use {
                        val id = it?.returnValue?.getOrThrow()
                        ResponseEntity(ResponseModel(Messages.CONTRACT_DRAFT_CREATED_SUCCESSFULY, HttpStatus.OK, id
                                ?: ""), HttpStatus.OK)
                    }
                }
            }
        } catch (e: Exception) {
            ResponseEntity(CustomError(formatErrorMsg(e.message), HttpStatus.CONFLICT), HttpStatus.BAD_REQUEST)
        }
    }


    @GetMapping("/getContracts")
    fun getContracts(@RequestParam(value = "pageNumber", required = true) pageNumber: Int,
                     @RequestParam(value = "userId", required = true) userId: String): ResponseEntity<Any> {
        return try {
            val flowHandle = rpc.getClient()?.startFlow(AllContractFlow::Contract,
                    pageNumber, userId)
            flowHandle.use {
                val pairData = it?.returnValue?.getOrThrow()
                ResponseEntity(ItemsListResponseModel(Messages.SUCCESS, HttpStatus.OK, pairData?.first
                        ?: 0, pairData?.second
                ), HttpStatus.OK)
            }
        } catch (ex: Exception) {
            ResponseEntity(CustomError("${Messages.SOMETHING_WENT_WRONG} ${ex.message}", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND)
        }
    }


    @GetMapping("/getHealthCareProfessionalContracts")
    fun getHealthCareProfessionalContracts(@RequestParam(value = "pageNumber", required = true) pageNumber: Int,
                     @RequestParam(value = "userId", required = true) userId: String): ResponseEntity<Any> {
        return try {
            val flowHandle = rpc.getClient()?.startFlow(GetHealthCareProfessionalContractsFlow::Contract,
                    pageNumber, userId)
            flowHandle.use {
                val pairData = it?.returnValue?.getOrThrow()
                ResponseEntity(ItemsListResponseModel(Messages.SUCCESS, HttpStatus.OK, pairData?.first
                        ?: 0, pairData?.second
                ), HttpStatus.OK)
            }
        } catch (ex: Exception) {
            ResponseEntity(CustomError("${Messages.SOMETHING_WENT_WRONG} ${ex.message}", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND)
        }
    }


    @GetMapping("/get-contract")
    fun getContract(contractId: String, userId: String): ResponseEntity<Any> {
        return try {


            val flowHandle = rpc.getClient()?.startFlow(GetContractContractFlow::Contract,
                    contractId, userId)
            flowHandle.use {
                val data = it?.returnValue?.getOrThrow()
                ResponseEntity(ResponseModel(Messages.SUCCESS, HttpStatus.OK, data), HttpStatus.OK)
            }
        } catch (e: Exception) {
            ResponseEntity(CustomError(formatErrorMsg(e.message), HttpStatus.CONFLICT), HttpStatus.BAD_REQUEST)
        }
    }

    @PutMapping("/update-contract-status")
    fun updateContractStatus(@RequestBody request: UpdateStatusContract): ResponseEntity<Any> {

        return try {
            val flowHandle = rpc.getClient()?.startFlow(FlowUpdateContractStatusFlow::UpdateContractStatus,
                    request)
            flowHandle.use {
                val message = it?.returnValue?.getOrThrow()
                ResponseEntity(ResponseModel(Messages.CONTRACT_STATUS_UPDATED_SUCCESSFULY, HttpStatus.OK, message), HttpStatus.OK)
            }
        } catch (e: Exception) {
            ResponseEntity(CustomError(formatErrorMsg(e.message), HttpStatus.CONFLICT), HttpStatus.BAD_REQUEST)
        }
    }

    @PutMapping("/update-contract-version")
    fun updateContractVersion(@RequestBody request: UpdateHealthcareaContractModel): ResponseEntity<Any> {

        return try {
            val flowHandle = rpc.getClient()?.startFlow(NewHealthcareVersionContractFlow::UpdateHealthcareContractVersion,
                    request)
            flowHandle.use {
                val message = it?.returnValue?.getOrThrow()
                ResponseEntity(ResponseModel(Messages.CONTRACT_VERSION_UPDATED_SUCCESSFULY, HttpStatus.OK, message), HttpStatus.OK)
            }
        } catch (e: Exception) {
            ResponseEntity(CustomError(formatErrorMsg(e.message), HttpStatus.CONFLICT), HttpStatus.BAD_REQUEST)
        }
    }

}