package com.debut.client.api.treatmentoperations

import com.debut.client.WrapperRpcClient
import com.debut.client.common.CommonMethods
import com.debut.client.common.Messages
import com.debut.clientmodel.treatment.TreatmentModel
import com.debut.clientmodel.treatment.TreatmentUpdateModel
import com.debut.flows.outcomes.AllOutComesFlow
import com.debut.flows.treatment.AllTreatmentComesFlow
import com.debut.flows.treatment.FlowCreateTreatment
import com.debut.flows.treatment.FlowUpdateTreatment
import com.debut.responsemodel.CustomError
import com.debut.responsemodel.ItemsListResponseModel
import com.debut.responsemodel.ListResponseModel
import com.debut.responsemodel.ResponseModel
import com.debut.states.treatment.TreatmentState
import com.debut.utility.Utils
import io.swagger.annotations.Api
import net.corda.core.messaging.startFlow
import net.corda.core.node.services.vault.*
import net.corda.core.utilities.getOrThrow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@Api(value = "TreatmentOperations", description = "Handle treatment operations")
class TreatmentOperations : CommonMethods() {


    @Autowired
    lateinit var rpc: WrapperRpcClient


    @PostMapping("/createTreatment")
    fun createTreatment(@RequestBody request: TreatmentModel): ResponseEntity<Any> {

        return try {


            //call flow to create new treatment
            val flowHandle = rpc.getClient()?.startFlow(FlowCreateTreatment::CreateTreatment,
                    request)

            flowHandle.use { flowHandle?.returnValue?.getOrThrow() }
            val id = flowHandle?.returnValue?.get()
            ResponseEntity(ResponseModel(Messages.TREATMENT_CREATED_SUCCESSFULY, HttpStatus.OK, id
                    ?: ""), HttpStatus.OK)

        } catch (e: Exception) {
            ResponseEntity(CustomError(formatErrorMsg(e.message), HttpStatus.CONFLICT), HttpStatus.BAD_REQUEST)
        }
    }

    @PutMapping("/updateTreatment")
    fun updateUser(@RequestBody request: TreatmentUpdateModel): ResponseEntity<Any> {

        return try {
            val flowHandle = rpc.getClient()?.startFlow(FlowUpdateTreatment::UpdateTreatment,
                    request)

            flowHandle.use { flowHandle?.returnValue?.getOrThrow() }
            val message = flowHandle?.returnValue?.get()
            ResponseEntity(ResponseModel(Messages.TREATMENT_UPDATED_SUCCESSFULY, HttpStatus.OK, message
            ), HttpStatus.OK)

        } catch (e: Exception) {
            ResponseEntity(CustomError(formatErrorMsg(e.message), HttpStatus.CONFLICT), HttpStatus.BAD_REQUEST)
        }
    }

    //
//
//

    @GetMapping("/get-treatment")
    fun getTreatment(treatmentId: String): ResponseEntity<Any> {
        return try {
            val userData = getTreatment(treatmentId, rpc)
            ResponseEntity(ListResponseModel(Messages.SUCCESS, HttpStatus.OK, userData), HttpStatus.OK)
        } catch (ex: Exception) {

            ResponseEntity(CustomError("${Messages.SOMETHING_WENT_WRONG} ${ex.message}", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND)
        }
    }


    @GetMapping("/get-all-treatment")
    fun getAllTreatment(@RequestParam(value = "pageNumber", required = true) pageNumber: Int): ResponseEntity<Any> {
        return try {
            val flowHandle = rpc.getClient()?.startFlow(AllTreatmentComesFlow::Treatement,
                    pageNumber)
            flowHandle.use {
                val pairData = it?.returnValue?.getOrThrow()
                ResponseEntity(ItemsListResponseModel(Messages.SUCCESS, HttpStatus.OK, pairData?.first
                        ?: 0, pairData?.second
                ), HttpStatus.OK)
            }
        } catch (ex: Exception) {
            ResponseEntity(CustomError("${Messages.SOMETHING_WENT_WRONG} ${ex.message}", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND)
        }
    }


    private fun getTreatmentList(pageNumber: Int): List<TreatmentState> {
        val sortAttribute = SortAttribute.Standard(Sort.CommonStateAttribute.STATE_REF_TXN_ID)
        val queryCriteria = QueryCriteria.VaultQueryCriteria()
        val result = rpc.getClient()?.vaultQueryBy(queryCriteria, PageSpecification(pageNumber, Utils.PAGE_LIMIT),
                Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))),
                TreatmentState::class.java)
        val listOfTreatments = ArrayList<TreatmentState>()
        //remove party information from state
        result?.let {
            for (asset in it.states) {
                val item = asset.state.data
                item.partyList = ArrayList()
                listOfTreatments.add(item)
            }
        }
        return listOfTreatments
    }


}

