package com.debut.client.api.users

import com.debut.client.WrapperRpcClient
import com.debut.client.common.CommonMethods
import com.debut.client.common.Messages
import com.debut.client.common.Messages.Companion.USER_EXISTS
import com.debut.clientmodel.user.HealthcareUser
import com.debut.flows.user.FlowCreateUser
import com.debut.flows.user.FlowUserDelete
import com.debut.flows.user.FlowUserUpdate
import com.debut.flows.user.UserDashboardFlow
import com.debut.responsemodel.CustomError
import com.debut.responsemodel.ListResponseModel
import com.debut.responsemodel.ResponseModel
import com.debut.states.user.UserState
import com.debut.utility.Utils
import io.swagger.annotations.Api
import net.corda.core.messaging.startFlow
import net.corda.core.node.services.vault.*
import net.corda.core.utilities.getOrThrow
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@Api(value = "user TreatmentOperations", description = "Handle user profile TreatmentOperations")
class UserOperations : CommonMethods() {


    @Autowired
    lateinit var rpc: WrapperRpcClient


    @PostMapping("/createUser")
    fun createUser(@RequestBody request: HealthcareUser): ResponseEntity<Any> {

        return try {

            //check if user already exists with given id
            if ((!checkIfUserExists(request._id, rpc))) {
                ResponseEntity(CustomError("$USER_EXISTS=${request._id}", HttpStatus.BAD_REQUEST),
                        HttpStatus.CONFLICT)
            }
            //register a new user
            else {
                val flowHandle = rpc.getClient()?.startFlow(FlowCreateUser::UserRegistration,
                        request)

                flowHandle.use { flowHandle?.returnValue?.getOrThrow() }
                val id = flowHandle?.returnValue?.get()
                ResponseEntity(ResponseModel(Messages.USER_CREATED_SUCCESSFULY, HttpStatus.OK, id
                        ?: ""), HttpStatus.OK)
            }
        } catch (e: Exception) {
            ResponseEntity(CustomError(formatErrorMsg(e.message), HttpStatus.CONFLICT), HttpStatus.BAD_REQUEST)
        }
    }

    @PutMapping("/updateUser")
    fun updateUser(@RequestBody request: HealthcareUser): ResponseEntity<Any> {

        return try {
            val flowHandle = rpc.getClient()?.startFlow(FlowUserUpdate::UserUpdateInfo,
                    request)

            flowHandle.use { flowHandle?.returnValue?.getOrThrow() }
            val id = flowHandle?.returnValue?.get()
            ResponseEntity(ResponseModel(Messages.USER_PROFILE_UPDATED_SUCCESSFULY, HttpStatus.OK, id
                    ?: ""), HttpStatus.OK)

        } catch (e: Exception) {
            ResponseEntity(CustomError(formatErrorMsg(e.message), HttpStatus.CONFLICT), HttpStatus.BAD_REQUEST)
        }
    }


    @DeleteMapping("/deleteUser/{userId}")
    fun deleteUser(@PathVariable userId: String): ResponseEntity<Any> {

        return try {
            val flowHandle = rpc.getClient()?.startFlow(FlowUserDelete::UserDelete,
                    userId)

            flowHandle.use { flowHandle?.returnValue?.getOrThrow() }
            val id = flowHandle?.returnValue?.get()
            ResponseEntity(ResponseModel(Messages.USER_PROFILE_DELETED_SUCCESSFULY, HttpStatus.OK, id
                    ?: ""), HttpStatus.OK)

        } catch (e: Exception) {
            ResponseEntity(CustomError(formatErrorMsg(e.message), HttpStatus.CONFLICT), HttpStatus.BAD_REQUEST)
        }
    }


    @GetMapping("/get-user")
    fun getUserDetails(userId: String): ResponseEntity<Any> {
        return try {
            val userData = getUser(userId, rpc)
            ResponseEntity(ListResponseModel(Messages.SUCCESS, HttpStatus.OK, userData), HttpStatus.OK)
        } catch (ex: Exception) {

            ResponseEntity(CustomError("${Messages.SOMETHING_WENT_WRONG} ${ex.message}", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND)
        }
    }


    @GetMapping("/get-all-users")
    fun getAllIssuers(): ResponseEntity<Any> {
        return try {
            val dataList = getUsersList()
            ResponseEntity(ListResponseModel(Messages.SUCCESS, HttpStatus.OK, dataList), HttpStatus.OK)
        } catch (ex: Exception) {

            ResponseEntity(CustomError("${Messages.SOMETHING_WENT_WRONG} ${ex.message}", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND)
        }
    }


    @GetMapping("/get-dashboard-data")
    fun getDashboardData(@RequestParam(value = "userId", required = true) userId: String): ResponseEntity<Any> {
        return try {
            val flowHandle = rpc.getClient()?.startFlow(UserDashboardFlow::UserDashboard,
                    userId)
            flowHandle.use {
                val data = it?.returnValue?.getOrThrow()
                ResponseEntity(ResponseModel(Messages.SUCCESS, HttpStatus.OK, data
                ), HttpStatus.OK)
            }
        } catch (ex: Exception) {
            ResponseEntity(CustomError("${Messages.SOMETHING_WENT_WRONG} ${ex.message}", HttpStatus.NOT_FOUND), HttpStatus.NOT_FOUND)
        }
    }


    private fun getUsersList(): List<UserState> {
        val sortAttribute = SortAttribute.Standard(Sort.CommonStateAttribute.STATE_REF_TXN_ID)
        val queryCriteria = QueryCriteria.VaultQueryCriteria()


        val result = rpc.getClient()?.vaultQueryBy(queryCriteria, PageSpecification(DEFAULT_PAGE_NUM, Utils.PAGE_LIMIT),
                Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))),
                UserState::class.java)


        val listOfUsers = ArrayList<UserState>()
        //remove party information from state
        result?.let {
            for (asset in it.states) {
                val item = asset.state.data
                item.partyList = ArrayList()
                listOfUsers.add(item)
            }
        }
        return listOfUsers
    }


}

