package com.debut.client.common

//combine response messages
class Messages {
    companion object {


        val USER_CREATED_SUCCESSFULY = "User created  successfully"
        val USER_PROFILE_UPDATED_SUCCESSFULY = "user profile updated successfully"
        val USER_PROFILE_DELETED_SUCCESSFULY = "user profile delete successfully"
        val USER_EXISTS = "user profile already exists with given id"
        val DISEASE_NOT_FOUND = "No disease found with given id"
        val SUCCESS = "Success"
        val SOMETHING_WENT_WRONG = "Something went wrong"
        val USER_NOT_FOUND = "No user found with given id"
        val CREATED_BY_NOT_FOUND = "No creator found with given Id"
        val MANUFACTURER_NOT_FOUND = "No Manufacturer found with given Id"
        //for treatment operations

        val TREATMENT_CREATED_SUCCESSFULY = "Treatment created  successfully"
        val TREATMENT_UPDATED_SUCCESSFULY = "Treatment updated  successfully"
        val NO_TREATMENT_FOUND = "No Treatment found"

        //for disease operations

        val DISEASE_CREATED_SUCCESSFULY = "Disease created  successfully"
        //for outcomes
        val OUTCOME_CREATED_SUCCESSFULY = "Outcome created  successfully"
        val OUTCOME_UPDATED_SUCCESSFULY = "Outcome updated  successfully"
        val CONTRACT_CREATED_SUCCESSFULY = "Contract created  successfully"
        val CONTRACT_DRAFT_CREATED_SUCCESSFULY = "Contract Draft created  successfully"
        val CONTRACT_UPDATED_SUCCESSFULY = "Contract updated  successfully"
        val CONTRACT_PROFESSIONAL_DATA_UPDATED_SUCCESSFULY = "Contract  professional observation data updated  successfully"
        val CONTRACT_STATUS_UPDATED_SUCCESSFULY = "Contract status updated  successfully"
        val CONTRACT_VERSION_UPDATED_SUCCESSFULY = "Contract with new version updated  successfully"
        val NO_OUTCOME_FOUND = "No Outcome found"
        val NO_CONTRACT_FOUND = "No Contract found"


    }
}