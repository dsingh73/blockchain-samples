package com.debut.client.common

import com.debut.client.WrapperRpcClient
import com.debut.contracts.RegistrationContract
import com.debut.states.aggregation.AggregationLevelSchema
import com.debut.states.aggregation.AggregationLevelState
import com.debut.states.category.AssetCategoryState
import com.debut.states.Healthcarecontract.HealthcareContractState
import com.debut.states.Healthcarecontract.HealthcareContractSchema
import com.debut.states.disease.DiseaseSchema
import com.debut.states.disease.DiseaseState
import com.debut.states.outcome.OutcomeState
import com.debut.states.outcome.OutcomeStateSchema
import com.debut.states.outcomedefination.OutcomeDefinationSchema
import com.debut.states.outcomedefination.OutcomeDefinationState
import com.debut.states.treatment.TreatmentSchema
import com.debut.states.treatment.TreatmentState

import com.debut.states.user.UserSchema
import com.debut.states.user.UserState
import net.corda.core.contracts.ContractState
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.*
import net.corda.finance.contracts.DealState
import java.text.ParseException
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


open class CommonMethods {

    companion object {
        val queryCriteriaLimitUnconsumed = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.UNCONSUMED)
        val queryCriteriaLimitConsumed = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.CONSUMED)
        val queryCriteriaLimitAll = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.ALL)

    }


    //check if user already exists or not for given user id
    fun checkIfUserExists(userId: String, userType: Int, rpc: WrapperRpcClient): Boolean {
        val sortAttribute = SortAttribute.Standard(Sort.CommonStateAttribute.STATE_REF_TXN_ID)

        val userState = builder { UserSchema.PersistentUserState::userId.equal(userId) }
        val userState2 = builder { UserSchema.PersistentUserState::userRole.equal(userType) }

        val criteria = QueryCriteria.VaultCustomQueryCriteria(userState).and(QueryCriteria.VaultCustomQueryCriteria(userState2))
        val result = rpc.getClient()?.vaultQueryBy(criteria, PageSpecification(DEFAULT_PAGE_NUM, 1), Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))),
                UserState::class.java)
        return result?.states?.isEmpty() == true
    }

    //check if user already exists or not for given user id
    fun checkIfUserExists(userId: String, rpc: WrapperRpcClient): Boolean {
        val sortAttribute = SortAttribute.Standard(Sort.CommonStateAttribute.STATE_REF_TXN_ID)

        val userState = builder { UserSchema.PersistentUserState::userId.equal(userId) }

        val criteria = QueryCriteria.VaultCustomQueryCriteria(userState)
        val result = rpc.getClient()?.vaultQueryBy(criteria, PageSpecification(DEFAULT_PAGE_NUM, 1), Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))),
                UserState::class.java)
        return result?.states?.isEmpty() == true
    }

    //check if given disease id exists
    fun checkIfDiseaseExists(diseaseId: String, rpc: WrapperRpcClient): Boolean {
        val sortAttribute = SortAttribute.Standard(Sort.CommonStateAttribute.STATE_REF_TXN_ID)

        val userState = builder { DiseaseSchema.PersistentDiseaseState::diseaseId.equal(diseaseId) }

        val criteria = QueryCriteria.VaultCustomQueryCriteria(userState)
        val result = rpc.getClient()?.vaultQueryBy(criteria, PageSpecification(DEFAULT_PAGE_NUM, 1), Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))),
                DiseaseState::class.java)
        return result?.states?.isEmpty() == true
    }


    //check if given aggregation level id exists
    fun checkIfAggregationLevelExists(levelId: String, rpc: WrapperRpcClient): Boolean {
        val sortAttribute = SortAttribute.Standard(Sort.CommonStateAttribute.STATE_REF_TXN_ID)

        val userState = builder { AggregationLevelSchema.PersistentAggregationLevel::id.equal(levelId) }

        val criteria = QueryCriteria.VaultCustomQueryCriteria(userState)
        val result = rpc.getClient()?.vaultQueryBy(criteria, PageSpecification(DEFAULT_PAGE_NUM, 1), Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))),
                AggregationLevelState::class.java)
        return result?.states?.isEmpty() == true
    }


    //check if given outcome defination level id exists
    fun checkIfOutcomeDefinationExists(outcomeDefinationId: String, rpc: WrapperRpcClient): Boolean {
        val sortAttribute = SortAttribute.Standard(Sort.CommonStateAttribute.STATE_REF_TXN_ID)

        val userState = builder { OutcomeDefinationSchema.PersistentOutcomeDefination::id.equal(outcomeDefinationId) }

        val criteria = QueryCriteria.VaultCustomQueryCriteria(userState)
        val result = rpc.getClient()?.vaultQueryBy(criteria, PageSpecification(DEFAULT_PAGE_NUM, 1), Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))),
                OutcomeDefinationState::class.java)
        return result?.states?.isEmpty() == true
    }


    //checkuserType
    fun isUserAdmin(userId: String, rpc: WrapperRpcClient): Boolean {
        val sortAttribute = SortAttribute.Standard(Sort.CommonStateAttribute.STATE_REF_TXN_ID)
        val userState = builder { UserSchema.PersistentUserState::userId.equal(userId) }
        val criteria = QueryCriteria.VaultCustomQueryCriteria(userState)
        val result = rpc.getClient()?.vaultQueryBy(criteria, PageSpecification(DEFAULT_PAGE_NUM, 1), Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))),
                UserState::class.java)


        return result?.states?.let {
            if (it.isNotEmpty()) {
                it[0].state.data.role == 1 //1 for admin type
            } else {
                false
            }
        } ?: false
    }


    //validate dates for purchase securities between
    private fun checkForValidDate(startDate: String, endDate: String): Boolean {
        return try {
            val calendar = Calendar.getInstance()
            val currentDate = calendar.time.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
            val startOfferDate = LocalDate.parse(startDate, DateTimeFormatter.ofPattern(RegistrationContract.DATE_FORMAT))
            val endOfferDate = LocalDate.parse(endDate, DateTimeFormatter.ofPattern(RegistrationContract.DATE_FORMAT))

            return (currentDate.isAfter(startOfferDate) && currentDate.isBefore(endOfferDate))
        } catch (e: ParseException) {
            false
        }

    }


    fun formatErrorMsg(errorMessage: String?): String {
        val errors = errorMessage?.split(":")
        return errors?.let {
            if (errors.size == 4) {
                errors[2]
            } else {
                errorMessage
            }
        } ?: errorMessage ?: ""

    }

    //return user details
    fun getUser(userId: String, rpc: WrapperRpcClient): UserState? {
        val stateRef1 = builder { UserSchema.PersistentUserState::userId.equal(userId) }
        val sortAttribute = SortAttribute.Standard(Sort.CommonStateAttribute.STATE_REF_TXN_ID)
        val queryCriteria = QueryCriteria.VaultQueryCriteria().and(QueryCriteria.VaultCustomQueryCriteria(stateRef1))
        val result = rpc.getClient()?.vaultQueryBy(queryCriteria, PageSpecification(DEFAULT_PAGE_NUM, 1),
                Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))),
                UserState::class.java)?.states?.firstOrNull()

        return result?.let {
            it.state.data
        }
    }


    //return treatment details
    fun getTreatment(treatmentId: String, rpc: WrapperRpcClient): TreatmentState? {
        val stateRef1 = builder { TreatmentSchema.PersistentTreatmentState::treatmentId.equal(treatmentId) }
        val sortAttribute = SortAttribute.Standard(Sort.CommonStateAttribute.STATE_REF_TXN_ID)
        val queryCriteria = QueryCriteria.VaultQueryCriteria().and(QueryCriteria.VaultCustomQueryCriteria(stateRef1))
        val result = rpc.getClient()?.vaultQueryBy(queryCriteria, PageSpecification(DEFAULT_PAGE_NUM, 1),
                Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))),
                TreatmentState::class.java)?.states?.firstOrNull()

        return result?.let {
            val state = it.state.data
            state.partyList = ArrayList()
            state
        }
    }


    //return outcome details
    fun getOutcome(outcomeId: String, rpc: WrapperRpcClient): OutcomeState? {
        val stateRef1 = builder { OutcomeStateSchema.PersistentOutcome::id.equal(outcomeId) }
        val sortAttribute = SortAttribute.Standard(Sort.CommonStateAttribute.STATE_REF_TXN_ID)
        val queryCriteria = QueryCriteria.VaultQueryCriteria().and(QueryCriteria.VaultCustomQueryCriteria(stateRef1))
        val result = rpc.getClient()?.vaultQueryBy(queryCriteria, PageSpecification(DEFAULT_PAGE_NUM, 1),
                Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))),
                OutcomeState::class.java)?.states?.firstOrNull()

        return result?.let {
            val state = it.state.data
            state.partyList = ArrayList() //remove party information
            state
        }
    }


    //return contract details
    fun getContract(contractId: String, rpc: WrapperRpcClient): HealthcareContractState? {
        val stateRef1 = builder { HealthcareContractSchema.PersistentHealthcareContract::contractId.equal(contractId) }
        val sortAttribute = SortAttribute.Standard(Sort.CommonStateAttribute.STATE_REF_TXN_ID)
        val queryCriteria = QueryCriteria.VaultQueryCriteria().and(QueryCriteria.VaultCustomQueryCriteria(stateRef1))
        val result = rpc.getClient()?.vaultQueryBy(queryCriteria, PageSpecification(DEFAULT_PAGE_NUM, 1),
                Sort(setOf(Sort.SortColumn(sortAttribute, Sort.Direction.ASC))),
                HealthcareContractState::class.java)?.states?.firstOrNull()

        return result?.let {
            val state = it.state.data
            state.partyList = ArrayList() //remove party information
            state
        }
    }


}

