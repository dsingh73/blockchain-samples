package com.debut.client

import net.corda.client.rpc.CordaRPCClient
import net.corda.client.rpc.RPCException
import net.corda.core.messaging.CordaRPCOps
import net.corda.core.utilities.NetworkHostAndPort

class WrapperRpcClient {

    private  var rpcClient: CordaRPCOps?=null

    fun createClient(){
        // TODO remove this when CordaRPC gets proper connection retry, please
        var maxRetries = 100
        Thread.sleep(6000)
        do {
            try {
                println("trying to connect ................................")
                rpcClient= CordaRPCClient(NetworkHostAndPort.parse("localhost:10006")).start("user1", "test").proxy
                println("connected ................................")
                break
            } catch (ex: RPCException) {
                if (maxRetries-- > 0) {
                    println("again to connect ................................")
                    Thread.sleep(1000)
                } else {
                    println("failed to connect ................................")
                    throw ex
                }
            }
        } while (true)
    }
    fun getClient(): CordaRPCOps?{
       return rpcClient
    }

}