package com.debut.client


import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2


@Configuration
@EnableSwagger2
@EnableAutoConfiguration
class SwaggerConfig {
    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.debut.client"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(metadata())
    }
}


private fun metadata(): ApiInfo {
    return ApiInfoBuilder()//
            .title("Healthcare Swagger Api")//
            .description(
                    "Api to create Healthcare items etc.")//
            .version("1.0.0")//
            .license("MIT License").licenseUrl("http://opensource.org/licenses/MIT")//
            .contact(Contact(null, null, "mauriurraco@gmail.com"))//
            .build()
}
