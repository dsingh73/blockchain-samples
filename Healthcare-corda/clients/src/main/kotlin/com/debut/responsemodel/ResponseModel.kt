package com.debut.responsemodel

import org.springframework.http.HttpStatus

data class ResponseModel (override val message: String?, override val status: HttpStatus,val data:Any?=null):Response(message,status)