package com.debut.responsemodel

import org.springframework.http.HttpStatus

data class CustomError (val message: String?,val status: HttpStatus)