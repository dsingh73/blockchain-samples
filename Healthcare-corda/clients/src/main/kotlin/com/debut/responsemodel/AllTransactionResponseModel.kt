package com.debut.responsemodel

import org.springframework.http.HttpStatus

data class AllTransactionResponseModel (override val message: String?, override val status: HttpStatus, val totalInterestReceived:Double, val data:Any?=null):Response(message,status)