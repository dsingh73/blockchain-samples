package com.debut.responsemodel

import org.springframework.http.HttpStatus

data class ItemsListResponseModel(override val message: String?, override val status: HttpStatus, val totalItemCount: Int, val data: Any? = null) : Response(message, status)