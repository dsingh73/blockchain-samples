package com.debut.responsemodel

import org.springframework.http.HttpStatus

open  class Response (open val message: String?, open val status: HttpStatus)