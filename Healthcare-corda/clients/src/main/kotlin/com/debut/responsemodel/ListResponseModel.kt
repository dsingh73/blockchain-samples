package com.debut.responsemodel

import org.springframework.http.HttpStatus

data class ListResponseModel (val message: String?, val status: HttpStatus, val data:Any?)