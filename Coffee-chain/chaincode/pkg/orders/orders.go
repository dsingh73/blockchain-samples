// Package orders Related functions
package orders

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"

	"github.com/chaincode/coffee-chain/pkg/core/status"
	"github.com/chaincode/coffee-chain/pkg/core/utils"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/s7techlab/cckit/router"
)

// CreateOrder create the coffee-chain Coffee Order into the CouchDB
func CreateOrder(c router.Context) (interface{}, error) {
	// get the data from the request and parse it as structure
	data := c.Param(`data`).(Order)

	// set the default values for the fields
	data.DocType = utils.DocTypeOrder

	// Validate the inputed data
	err := data.Validate()
	if err != nil {
		if _, ok := err.(validation.InternalError); ok {
			return nil, err
		}
		return nil, status.ErrStatusUnprocessableEntity.WithValidationError(err.(validation.Errors))
	}

	// get the stub to use it for query and save
	stub := c.Stub()

	// check the order already exists or not
	queryString := fmt.Sprintf("{\"selector\":{\"order_no\":\"%s\",\"doc_type\":\"%s\"}}", data.OrderNo, utils.DocTypeOrder)
	alreadyExists, stateKey, _ := utils.Get(c, queryString, "")
	if alreadyExists != nil {
		// prepare the response body
		responseBody := utils.ResponseID{ID: stub.GetTxID()}
		// update the data and return the response
		return responseBody, c.State().Put(stateKey, data)
	}

	// prepare the response body
	responseBody := utils.ResponseID{ID: stub.GetTxID()}

	// Save the data and return the response
	return responseBody, c.State().Put(stub.GetTxID(), data)
}

// UpdateOrder update the coffee-chain Coffee Order into the CouchDB
func UpdateOrder(c router.Context) (interface{}, error) {
	// get the data from the request and parse it as structure
	data := c.Param(`data`).(Order)

	// Validate the inputed data
	err := data.Validate()
	if err != nil {
		if _, ok := err.(validation.InternalError); ok {
			return nil, err
		}
		return nil, status.ErrStatusUnprocessableEntity.WithValidationError(err.(validation.Errors))
	}

	// set the default values for the fields
	data.DocType = utils.DocTypeOrder

	// get the stub to use it for query and save
	stub := c.Stub()

	// check the order exists or not
	queryString := fmt.Sprintf("{\"selector\":{\"order_no\":\"%s\",\"doc_type\":\"%s\"}}", data.OrderNo, utils.DocTypeOrder)
	orderDetails, stateKey, err := utils.Get(c, queryString, fmt.Sprintf("Order with order number: %s does not exists!", data.OrderNo))
	if orderDetails == nil {
		data.CreatedAt = time.Now()
		responseBody := utils.ResponseID{ID: stub.GetTxID()}
		// create order
		return responseBody, c.State().Put(stub.GetTxID(), data)
	}

	// ummarshal the byte array to structure
	orderdata := Order{}
	err = json.Unmarshal(orderDetails, &orderdata)
	if err != nil {
		return nil, status.ErrInternal.WithError(err)
	}

	data.CreatedAt = orderdata.CreatedAt
	responseBody := utils.ResponseID{ID: stub.GetTxID()}

	// return the response
	return responseBody, c.State().Put(stateKey, data)
}

// GetHistoryForKey to get history
func GetHistoryForKey(c router.Context) (interface{}, error) {
	// get the data from the request and parse it as structure
	data := c.Param(`data`).(Key)

	// get the stub to use it for get history
	stub := c.Stub()
	var historyData bytes.Buffer
	historyData.WriteString("{\"history\": [")
	aArrayMemberAlreadyWritten := false

	historyIer, _ := stub.GetHistoryForKey(data.Key)
	for historyIer.HasNext() {
		// Add a comma before array members, suppress it for the first array member
		if aArrayMemberAlreadyWritten == true {
			historyData.WriteString(",")
		}

		modification, _ := historyIer.Next()
		historyData.WriteString(string(modification.Value))
		aArrayMemberAlreadyWritten = true
	}

	historyData.WriteString("]}")

	// return the response
	return historyData.Bytes(), nil
}

// GetOrders to get orders and suborders
func GetOrders(c router.Context) (interface{}, error) {
	// get the data from the request and parse it as structure
	data := c.Param(`data`).(OrderNo)

	// get the stub to use it for get orders
	stub := c.Stub()
	var orderData bytes.Buffer
	orderData.WriteString("{\"orders\": [")
	aArrayMemberAlreadyWritten := false
	orderQueryString := fmt.Sprintf("{\"selector\":{\"order_no\":\"%s\"}}", data.OrderNo)
	resultsIterator, err := stub.GetQueryResult(orderQueryString)
	// check if the query executed successfully?
	if err != nil {
		return nil, status.ErrInternal.WithError(err)
	}
	defer resultsIterator.Close()

	for resultsIterator.HasNext() {
		// Add a comma before array members, suppress it for the first array member
		if aArrayMemberAlreadyWritten == true {
			orderData.WriteString(",")
		}

		order, _ := resultsIterator.Next()
		orderData.WriteString(string(order.Value))
		aArrayMemberAlreadyWritten = true
	}

	orderData.WriteString("]}")

	// return the response
	return orderData.Bytes(), nil
}
