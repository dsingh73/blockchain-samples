'use strict';

const { FileSystemWallet, Gateway } = require('fabric-network');
const fs = require('fs');
const path = require('path');
var events = require('../../../helper/v1/events');

const ccpPath = path.resolve(__dirname, '.', '../..', 'connection.json');
const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
const ccp = JSON.parse(ccpJSON);

// Create a new file system based wallet for managing identities.
const walletPath = path.join(process.cwd(), 'wallet');
const wallet = new FileSystemWallet(walletPath);

class FabricOperation {

    /**
     * @api Query Transaction
     * @param string user username
     * @param string channel_name channel name to query
     * @param string chaincode_name  chaincode name
     * @param string function_name function name
     * @param json data data for query
     */
    async query(user, channel_name, chaincode_name, function_name, data = null) {
        try {
            await this.userExists(user);

            // Create a new gateway for connecting to our peer node.
            const gateway = new Gateway();
            await gateway.connect(ccp, { wallet, identity: user, discovery: { enabled: false } });

            // Get the network (channel) our contract is deployed to.
            const network = await gateway.getNetwork(channel_name);

            // Get the contract from the network.
            const contract = network.getContract(chaincode_name);

            let result;

            // Submit the specified transaction.
            if (data) {
                result = await contract.evaluateTransaction(function_name, JSON.stringify(data));
            }
            else {
                result = await contract.evaluateTransaction(function_name);
            }

            // Disconnect from the gateway.
            await gateway.disconnect();

            return Promise.resolve({
                status: 200,
                data: JSON.parse(result.toString())
            });
        } catch (error) {
            return Promise.reject(this.handleError(error));
        }
    }

    /**
     * @api Invoke Transaction
     * @param string user username
     * @param string channel_name channel name to query
     * @param string chaincode_name  chaincode name
     * @param string function_name function name
     * @param json data data for query
     */
    async invoke(user, channel_name, chaincode_name, function_name, data, order_ids) {
        try {

            await this.userExists(user);
            // Create a new gateway for connecting to our peer node.
            const gateway = new Gateway();
            await gateway.connect(ccp, { wallet, identity: user, discovery: { enabled: true } });

            // Get the network (channel) our contract is deployed to.
            const network = await gateway.getNetwork(channel_name);

            // Get the contract from the network.
            const contract = network.getContract(chaincode_name);

            // Submit the specified transaction.
            let result;
            result = await contract.submitTransaction(function_name, JSON.stringify(data));
            result = JSON.parse(result.toString());

            /**
             * @param {String} transactionId the name of the event listener
             * @param {Function} callback the callback function with signature (error, transactionId, status, blockNumber)
             * @param {Object} options
            **/
            network.addCommitListener(result.id, async (err, transactionId, status, blockNumber) => {
                if (err) {
                    return;
                }
                let update_order_data = {
                    transaction_id: transactionId,
                    transaction_status: status
                }

                if (function_name == "createOrder" || function_name == "updateOrder") {
                    events.emit('updateOrder', order_ids, update_order_data);
                } else {
                    events.emit('updateSubOrders', order_ids, update_order_data);
                }
            });


            // Disconnect from the gateway.
            await gateway.disconnect();

            return Promise.resolve({
                status: 200,
                data: result
            });
        } catch (error) {
            return Promise.reject(this.handleError(error));
        }
    }
}

module.exports = FabricOperation;